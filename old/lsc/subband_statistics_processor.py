# subband_statistics_processor.py: Module with functions for processing 
# LOFAR2 station subband statistic data
#
# -*- coding: utf-8 -*-
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.


"""  module subband_statistics_processor

This module contains functions for processing subband statistics.

The LOFAR2 station records subband statistics in a setup that is represented by:

 +--------+    +-----+    +-----+
 | RECV 0 | -> |     | -> | LCU | --+
 +--------+    |     |    +-----+   |
 +--------+    | SDP |              V
 | RECV 1 | -> |     |
 +--------+    |     |             SST
    :          |     |           file(s) -> this class
 +--------+    |     |                      for processing SSTs
 | RECV N | -> |     |
 +--------+    +-----+
 
 ^Receivers    ^Station Digital Processor
 
                          ^Local Control Unit with sst_writer

Boudewijn Hut
"""
__version__ = '0.1'

import h5py
from os import listdir
from os.path import isfile,join
import datetime
import numpy as np
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib import cm
import processing_lib as plib

# and set defaults
plt.rcParams['figure.figsize'] = [16, 10]
keyH = 'C' # key for High Band in antenna names, for DTS

def findSSTh5Files(filesDir='SSTs/', ext='.h5', pattern='sst'):
    '''
    Find all SST files in a certain directory.
    Filtering is done on extension and filename
    
    Input arguments:
    filesDir = directory to be searched.
               Default: 'SSTs/'
    ext      = extension of file.
               Default: '.h5'
    pattern  = sub-string of pattern that should be present in filename. Case insensitive.
               Default: 'sst'
               
    Output arguments:
    files = list of SST files, sorted
    '''
    if filesDir[-1] is not '/':
        filesDir += '/'
    # check extension and filename
    files = [filesDir + f for f in listdir(filesDir) if isfile(join(filesDir,f)) and not f.split('.')[-1] == ext and pattern in f.lower()]
    files.sort()
    return files

def loadSSTh5Integration(grp, do_check=True, silent=True):
    '''
    Load a single integration from an SST file.
    
    If the interface of the sst_writer changes, this (and only this) function should be changed accordingly.
    Some basic checks are performed and a warning is printed if a check fails.
    
    Input arguments:
    grp  = Group object representing the single integration
    do_check = flag to perform basic checks, boolean. Default: True
    silent   = flag to provide debugging data. Default: True
    
    Output arguments:
    time = datetime of this integration, datetime object
    data = data of this integration, numpy array
    '''
    if not silent:
        print('Group has the following keys:')
        print(grp.keys())
    # get data
    try:
        time = datetime.datetime.strptime(grp.name, "/SST_%Y-%m-%dT%H:%M:%S.%f%z")
    except ValueError:
        time = datetime.datetime.strptime(grp.name, "/SST_%Y-%m-%dT%H:%M:%S.%f+00:00")
    data = np.array(grp['values'])
    payload_errors = list(grp['nof_payload_errors'])
    nof_valid_payloads = list(grp['nof_valid_payloads'])
    # perform basic checks
    if do_check:
        if any(payload_errors):
            print('Warning! Payload errors (%s) detected for group "%s"' % (payload_errors, grp.name))
    # return
    if not silent:
        print('Group time: %s' % time)
        print('Group payload_errors: %s' % payload_errors)
        print('Group nof_valid_payloads: %s' % nof_valid_payloads)
        print('Group data.shape: %s' % str(data.shape))
    return time, data

def loadSSTh5(file, silent=True, debug=False):
    '''
    Load the subband statistics from the hdf5 formatted file.
    
    The file contains one or more integrations, as written by the statistics_writer.py for subband statistics.
    
    SST file - SST integration 0
             - SST integration 1
             - SST integration 2
    '''
    # check extension to be h5
    if file.split('.')[-1] != 'h5':
        raise Exception('File (%s) is not an h5 file' % file)
    # check file to be subband statistics
    if 'sst' not in file.lower():
        raise Exception('File (%s) does not contain subband statistics' % file)
    # open h5 file
    with h5py.File(file, 'r') as fid:
        n_integrations = len(fid.keys())
        if not silent:
            print('File %s has %d integrations' % (file, len(fid.keys())))
        if debug:
            print('File has the following keys:')
            print(list(fid.keys()))
        # loop over all integrations
        time = []
        data = []
        for this_sst in fid.keys():
            this_time, this_data = loadSSTh5Integration(fid[this_sst])
            time.append(this_time)
            data.append(this_data)
    data = np.array(data)
    # FIXME: frequency axis should be in the header of the file, based on RECV/RCU configuration
#    cst_n_sub = 512 # number of subbands as output of subband generation in firmware
#    cst_fs = 100e6 # sampling frequency in Hz
#    freq = np.arange(cst_n_sub)*cst_fs/cst_n_sub # low band frequency axis
#    #freq = 200e6 - freq # high band frequency axis
    freq = plib.get_frequency_for_band_name('LB') 
    # FIXME: antenna names should be in the header of the file
    rec = ['LBA0X', 'LBA0Y', 'LBA1X', 'LBA1Y', 'LBA2X', 'LBA2Y', 'LBA3X', 'LBA3Y', 'LBA4X', 'LBA4Y', 'LBA5X', 'LBA5Y',\
           'LBA6X', 'LBA6Y', 'LBA7X', 'LBA7Y', 'LBA8X', 'LBA8Y'] + [None]*6 +\
           ['C0X', 'C0Y', 'C1X', 'C1Y', 'C2X', 'C2Y'] + [None]*162
    return (data, time, rec, freq)

def removeUnconnectedInputs(data_in, rec_in):
    '''
    remove unconnected inputs from the data
    '''
    connectedInputs = [idx for idx, r in enumerate(rec_in) if r is not None]
    # time (first) and frequency (third) dimensions are unaffected
    data_out = data_in[:,connectedInputs,:]
    rec_out = [r for r in rec_in if r]
    return data_out, rec_out

# separate low band data from high band data
def separateBands(data, rec, freq, keyL='L', keyH='H'):
    '''
    separate low and high bands
    frequency axis is generated (FIXME, should come from data)
    
    Input arguments:
    data    = data as returned by loadSSTh5
    rec     = rec  as returned by removeUnconnectedInputs
    freq    = freq as returned by loadSSTh5, ignored for now (FIXME)
    keyL    = key for low band to be used to filter receiver names (rec)
    keyH    = key for high band to be used to filter receiver names (rec)
    
    Output arguments:
    (-- low --), (-- high --) output arguments as returned by loadSSTh5, per band
    
    Example:
    file = 'SSTs/SST_2022-05-21-15-34-58.h5'
    data, time, rec, freq = loadSSTh5(file)
    data, rec = removeUnconnectedInputs(data, rec)
    (dataL, recL, freqL), (dataH, recH, freqH) = separateBands(data, rec, freq, keyL='L', keyH='C')
    
    '''
    # separate data
    recIdxsL = [idx for idx, r in enumerate(rec) if keyL in r]
    recIdxsH = [idx for idx, r in enumerate(rec) if keyH in r]
    dataL = data[:,recIdxsL,:]
    dataH = data[:,recIdxsH,:]
    # separate receiver names
    recL = [r for idx, r in enumerate(rec) if keyL in r]
    recH = [r for idx, r in enumerate(rec) if keyH in r]
    # generate freq axes (FIXME, should come from data)
#    cst_n_sub = 512 # number of subbands as output of subband generation in firmware
#    cst_fs = 100e6  # sampling frequency in Hz
#    freqL = np.arange(cst_n_sub)*cst_fs/cst_n_sub # low band frequency axis
#    freqH = 200e6 - np.arange(cst_n_sub)*cst_fs/cst_n_sub # high band frequency axis
    freqL = plib.get_frequency_for_band_name('LB')
    freqH = plib.get_frequency_for_band_name('HB1')
    return (dataL, recL, freqL), (dataH, recH, freqH)

def getDataFromFile(file, keyL='L', keyH='H'):
    data, time, rec, freq = loadSSTh5(file)
    data, rec = removeUnconnectedInputs(data, rec)
    (dataL, recL, freqL), (dataH, recH, freqH) = separateBands(data, rec, freq, keyL=keyL, keyH=keyH)
    # sanity check of sst data
    # first dimension should be the time axis
    if len(time) != dataL.shape[0]:
        raise Exception('Length of first dimension of data does not equal length of time axis (low band)')
    if len(time) != dataH.shape[0]:
        raise Exception('Length of first dimension of data does not equal length of time axis (high band)')
    # second dimension is number of inputs
    if len(recL) != dataL.shape[1]:
        raise Exception('Length of second dimension of data does not equal number of receiver names (low band)')
    if len(recH) != dataH.shape[1]:
        raise Exception('Length of second dimension of data does not equal number of receiver names (high band)')
    # third dimension is frequency
    if len(freqL) != dataL.shape[2]:
        raise Exception('Length of third dimension of data does not equal length of frequency axis (low band)')
    if len(freqH) != dataH.shape[2]:
        raise Exception('Length of third dimension of data does not equal length of frequency axis (high band)')
    return (dataL, recL, freqL), (dataH, recH, freqH), time

def dimensionsMatch(data1, rec1, freq1, data2, rec2, freq2):
    # check that dimensions rec and freq are the same
    if not data1.shape[1:] == data2.shape[1:]:
        print("Data has different dimensions")
        return False
    # check that axis rec and freq are the same
    if (not rec1 == rec2) or (not (freq1 == freq2).all()):
        print("Frequency or receiver names are different")
        return False
    return True

def appendDataFromFiles(data_tuple_in, files, keyL='L', keyH='H', silent=True):
    '''
    Append data from one or more files to the data of another file
    
    Input arguments:
    data_tuple_in  = tuple: output of getDataFromFile method in a tuple:
                     (-- low data, rec, freq --), (-- high data, req, freq --), time
    files          = list of files to be loaded
    keyL, keyH     = see separateBands method
    
    Output arguments:
    data_tuple_out = tuple as data_tuple_in, with appended data
    '''
    # check input
    if len(files) == 0:
        raise Exception("variable files should not have length 0")
    if type(data_tuple_in) is not tuple:
        raise Exception("Variable data_tuple_in should be a tuple")
    if type(data_tuple_in[0]) is not tuple:
        raise Exception("Variable data_tuple_in[0] should be a tuple, representing low band data, req and freq")
    if type(data_tuple_in[1]) is not tuple:
        raise Exception("Variable data_tuple_in[1] should be a tuple, representing high band data, req and freq")
    if type(data_tuple_in[2]) is not list:
        raise Exception("Variable data_tuple_in[2] should be a list, representing time")
    # start loading data
    ((dataL, recL, freqL), (dataH, recH, freqH), time) = data_tuple_in
    files.sort()
    for file in files:
        if not silent:
            print("Loading data from file %s" % file)
        (this_dataL, this_recL, this_freqL), (this_dataH, this_recH, this_freqH), this_time = getDataFromFile(file, keyL=keyL, keyH=keyH)
        # do some checks on the data
        if not dimensionsMatch(dataL, recL, freqL, this_dataL, this_recL, this_freqL):
            print("Issue found for the low band. Skipping file: %s" % file)
            continue
        if not dimensionsMatch(dataH, recH, freqH, this_dataH, this_recH, this_freqH):
            print("Issue found for the high band. Skipping file: %s" % file)
            continue
        dataL = np.append(dataL, this_dataL, axis=0)
        dataH = np.append(dataH, this_dataH, axis=0)
        time = np.append(time, this_time)
    return ((dataL, recL, freqL), (dataH, recH, freqH), time)

def plotTimeAxis(time, label='Time', figFile=None, subtitle=None, ax=None):
    '''
    Plot the time axis for inspection
    
    Input arguments:
    time     = list of datetime.datetime, time axis
    label    = string, label of time to be shown along axis. Default: 'Time'
    figFile  = string of filename where plot should be save. Default: None.
               If figFile is not set, plot is not saved to file.
    subtitle = string for subtitle on plot. Default: None.
    ax       = axes handle for plot. Default: None.
               If ax is not set, a new figure is generated
    
    Output arguments:
    none
    '''
    # create annotation text (showing properties of time axis)
    tstep = np.unique(np.diff(time))
    t0 = time[0]
    t1 = time[-1]
    dt = time[-1] - time[0]
    if len(tstep) > 1:
        print('WARNING - Data is not on a regular grid. Found following differences:')
        print(tstep)
    annotationText = ''
    annotationText += 'Start: %s\n' % t0.strftime('%Y-%m-%d %H:%M:%S.%f (%Z)')
    annotationText += 'Stop: %s\n' % t1.strftime('%Y-%m-%d %H:%M:%S.%f (%Z)')
    annotationText += 'Duration: %s (%s sec)\n' % (dt, dt.seconds)
    annotationText += 'Stepsize: %s (%s sec)\n' % (tstep[0], tstep[0].seconds)
    annotationText += 'Number of samples: %s' % len(time)
    # make plot
    startDate = '%s-%s-%s' % (time[0].day, time[0].month, time[0].year)
    if ax is None:
        fig, ax = plt.subplots(figsize=(12, 6))
    ax.plot(time, color='gray', alpha=0.2)
    ax.scatter(np.arange(len(time)), time, label=annotationText)
    ax.legend()
    ax.grid()
    ax.set_xlabel('Sample')
    ax.set_ylabel(label)
    title = 'Time axis (start on %s)' % startDate
    if subtitle:
        title += '\n%s' % subtitle
    ax.set_title(title)
    #ax.yaxis.set_major_locator(mdates.MinuteLocator(interval=10)) # tick every 10 mins
    ax.yaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))   # format of y tick labels
    if figFile:
        plt.savefig(figFile)
    plt.show()
    
def plotFreqAxes(freqs, label='Frequency (Hz)', figFile=None, subtitle=None, ax=None):
    '''
    Plot the frequency axis for inspection.
    Unit of the input data should be Hz. For convenience MHz is shown in the figure.
    
    Input arguments:
    freqs    = numpy array with frequency axis in Hz
    freqs    = list of numpy arrays with frequency axes in Hz
    label    = string, label of frequency to be shown along axis. Default: 'Frequency (Hz)'
    figFile  = string of filename where plot should be save. Default: None.
               If figFile is not set, plot is not saved to file.
    subtitle = string for subtitle on plot. Default: None.
    ax       = axes handle for plot. Default: None.
               If ax is not set, a new figure is generated
    
    Output arguments:
    none
    '''
    # make sure it is a list
    if not isinstance(freqs, list):
        freqs = [freqs]
    # create annotation text (showing properties of time axis)
    annotationTexts = []
    for freq in freqs:
        fstep = np.unique(np.diff(freq))
        f0 = freq[0]
        f1 = freq[-1]
        df = np.abs(freq[-1] - freq[0])
        if len(fstep) > 1:
            print('WARNING - Data is not on a regular grid. Found following differences:')
            print(tstep)
        annotationText = ''
        annotationText += 'Start: %.3f MHz\n' % (f0/1e6)
        annotationText += 'Stop: %.3f MHz\n' % (f1/1e6)
        annotationText += 'Bandwidth: %.3f MHz\n' % (df/1e6)
        annotationText += 'Stepsize: %s MHz\n' % (fstep[0]/1e6)
        annotationText += 'Number of samples: %s' % len(freq)
        annotationTexts.append(annotationText)
    if ax is None:
        fig, ax = plt.subplots(figsize=(12, 6))
    for annotationText, freq in zip(annotationTexts, freqs):
        ax.plot(freq/1e6, color='gray', alpha=0.2)
        ax.scatter(np.arange(len(freq)), freq/1e6, label=annotationText)
    ax.legend()
    ax.grid()
    ax.set_xlabel('Sample')
    ax.set_ylabel(label)
    title = 'Frequency axis'
    if subtitle:
        title += '\n%s' % subtitle
    ax.set_title(title)
    if figFile:
        plt.savefig(figFile)
    plt.show()

def plotSubbandStatistics(data, time, rec, freq, timeIdx = 0, recIdx = None, ax=None, figFile=None, ymin=None, ymax=None):
    '''
    Make the standard subband statistics plot for a single time integration
    
    Input arguments:
    data    = data as returned by loadSSTh5
    time    = time as returned by loadSSTh5
    rec     = rec as returned by loadSSTh5
    freq    = freq as returned by loadSSTh5
    timeIdx = time index for which the plot should be made (default: first)
    recIdx  = receiver index for which the plot should be made
              (default: all)
    ax      = ax handle where the plot should be made
              (default: ax of new figure)
    ymin    = minimum y axis value (default: None)
    ymax    = maximum y axis value (default: None)
    
    Output arguments:
    none
    '''
    if recIdx is None:
        recIdx = range(len(data[0]))
        title = 'Subband Statistics\n%s' % time[timeIdx]
    else:
        title = 'Subband Statistics for Receiver %s\n%s' % (rec[recIdx], time[timeIdx])
    if ax is None:
        fig, ax = plt.subplots(figsize=(12, 6))
    ax.plot(freq/1e6, 10*np.log10(data[timeIdx][recIdx,:]).T)
    ax.grid()
    ax.set_xlabel('Frequency (MHz)')
    ax.set_ylabel('Power (dB)')
    ax.set_title(title)
    if ymin or ymax:
        ymin = ymin if ymin is not None else ax.get_ylim()[0]
        ymax = ymax if ymax is not None else ax.get_ylim()[1]
        ax.set_ylim((ymin, ymax))
    if figFile:
        plt.savefig(figFile)

def plotSSTsForBothBands(dataL, recL, freqL, dataH, recH, freqH, time, recIdxL = None, recIdxH = None, figFile=None, ymin=None, ymax=None):
    fig, ax = plt.subplots(figsize=(12, 6))
    plotSubbandStatistics(dataL, time, recL, freqL, recIdx=recIdxL, ax=ax, ymin=ymin, ymax=ymax)
    plotSubbandStatistics(dataH, time, recH, freqH, recIdx=recIdxH, ax=ax)
    ax.grid()
    if figFile:
        plt.savefig(figFile)

def plotWaterfall(data, time, rec, freq, recIdx = 0, ax=None, plotColorbar=True, vmin=None, vmax=None, transpose=False):
    '''
    Make the waterfall subband statistics plot for a single receiver
    
    Input arguments:
    data    = data as returned by loadSSTh5
    time    = time as returned by loadSSTh5
    rec     = rec as returned by loadSSTh5
    freq    = freq as returned by loadSSTh5
    recIdx  = recevier index for which the plot should be made
    ax      = ax handle where the plot should be made
    plotColorbar = boolean to plot the colorbar (only works for ax=None)
    vmin    = argument as used by pcolor (default: None)
    vmax    = argument as used by pcolor (default: None)
    transpose = False --> frequency along x, time along y (default)
                True  --> time along x, frequency along y
    
    Output arguments:
    none
    '''
    if not isinstance(rec, list):
        raise Exception("Input argument rec should be a list")
    if ax is not None:
        plotColorbar = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(12, 6))
        fig.suptitle('Waterfall plot')
    x = freq/1e6 if transpose is False else time
    y = time if transpose is False else freq/1e6 
    z = data[:,recIdx,:] if transpose is False else data[:,recIdx,:].T
    z = 10*np.log10(z)
    p = ax.pcolor(x, y, z, cmap='inferno', vmin=vmin, vmax=vmax)
    if plotColorbar is True:
        fig.colorbar(p)
    startDate = '%s-%s-%s' % (time[0].day, time[0].month, time[0].year)
    if transpose is False:
        ax.set_xlabel('Frequency (MHz)')
        ax.set_ylabel('Time (start on %s)' % startDate)
        ax.invert_yaxis()
        #ax.yaxis.set_major_locator(mdates.MinuteLocator(interval=5))
        ax.yaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    else:
        ax.set_xlabel('Time (start on %s)' % startDate)
        ax.set_ylabel('Frequency (MHz)')
        #ax.xaxis.set_major_locator(mdates.MinuteLocator(interval=5))
        ax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    ax.set_title('Receiver: %s' % rec[recIdx])

def splitDataAlongPolarisations(data, rec, keyX='X', keyY='Y'):
    '''
    Split data in X and Y polarisation, based on receiver names (containing 'X' or 'Y')
    
    Input argumnets
    data    = data as returned by loadSSTh5
    rec     = rec as returned by loadSSTh5
    keyX    = character to filter rec for X polarisation (default: 'X')
    keyY    = character to filter rec for Y polarisation (default: 'Y')
    
    Output arguments
    dataX   = filtered data for X pol
    recX    = filtered rec for X pol
    dataY   = filtered data for Y pol
    recY    = filtered rec for Y pol
    '''
    recX = [r for r in rec if keyX in r]
    recY = [r for r in rec if keyY in r]
    shapeX = list(data.shape); shapeX[1] = len(recX); shapeX = tuple(shapeX)
    shapeY = list(data.shape); shapeY[1] = len(recY); shapeY = tuple(shapeY)
    dataX = np.empty(shapeX)
    dataY = np.empty(shapeY)
    for idx, thisRec in enumerate(recX):
        rIdx = rec.index(thisRec) # receiver index in original data variable
        dataX[:,idx,:] = data[:,rIdx,:]
    for idx, thisRec in enumerate(recY):
        rIdx = rec.index(thisRec)
        dataY[:,idx,:] = data[:,rIdx,:]
    return dataX, recX, dataY, recY

def inspectionPlotsFromSubbandStatisticsFiles(files, keyL='L', keyH='H', plotFreqs=147e6, inspectionPlotDir='inspection_plots', obstag=''):
    '''
    Combine multiple subband statistics file and make inspection plot for the combined data set
    
    Input arguments:
    files = list of files
    plotFreqs = frequency bin to be plotted (for plots over time for given frequency). Default: 147e6 Hz
    inspectionPlotsDir = string of folder where to place figures
    
    Output arguments:
    none
    '''
    # make sure folder exists
    plib.create_folder(inspectionPlotDir)
    # some conversions
    if not isinstance(plotFreqs, list): # make sure it is a list
        plotFreqs = [plotFreqs]
    band_names, subband_indices = plib.get_band_name_and_subband_index_for_frequency(plotFreqs)
    plotFreqIdx = subband_indices.tolist()
    basename = obstag.lower().replace(" ", "_")
    # load data
    (dataL, recL, freqL), (dataH, recH, freqH), time = getDataFromFile(files[0], keyL=keyL, keyH=keyH)
    ((dataL, recL, freqL), (dataH, recH, freqH), time) = appendDataFromFiles(((dataL, recL, freqL),
                                                                              (dataH, recH, freqH), time),
                                                                             files[1:], keyH=keyH)
    # make plots
    plotTimeAxis(time, label='Time (UTC)',
                      figFile='%s/%s_timeAxis.png' % (inspectionPlotDir, basename),
                      subtitle=obstag)
    plotFreqAxes([freqL, freqH],
                      figFile='%s/%s_freqAxis.png' % (inspectionPlotDir, basename),
                      subtitle=obstag)
    plotSubbandStatistics(dataL, time, recL, freqL,
                                         figFile='%s/%s_low_band_receivers_spectra.png' % (inspectionPlotDir, basename))
    plotSubbandStatistics(dataH, time, recH, freqH,
                                         figFile='%s/%s_high_band_receivers_spectra.png' % (inspectionPlotDir, basename))
    plotSSTsForBothBands(dataL, recL, freqL, dataH, recH, freqH, time,
                                         figFile='%s/%s_both_bands_spectra.png' % (inspectionPlotDir, basename))
    waterfallPlotsFromSubbandStatistics(dataL,time, recL, freqL,
                                         subtitle=['Low Band Receivers', obstag],
                                         figFile='%s/%s_low_band_receivers_waterfall.png' % (inspectionPlotDir, basename))
    waterfallPlotsFromSubbandStatistics(dataH, time, recH, freqH,
                                         subtitle=['High Band Receivers', obstag],
                                         figFile='%s/%s_high_band_receivers_waterfall.png' % (inspectionPlotDir, basename))
    for fIdx in plotFreqIdx:
        timePlotsFromSubbandStatistics(dataL,time, recL, freqL, freqIdx=fIdx,
                                             subtitle=['Low Band Receivers, Frequency %.2f MHz' % (freqL[fIdx]/1e6), 
                                                       obstag],
                                             figFile='%s/%s_low_band_receivers_timeplot_%.0dMHz.png' %
                                               (inspectionPlotDir, basename, freqL[fIdx]/1e6))
        timePlotsFromSubbandStatistics(dataH, time, recH, freqH, freqIdx=fIdx,
                                             subtitle=['High Band Receivers, Frequency %.2f MHz' % (freqL[fIdx]/1e6),
                                                       obstag],
                                             figFile='%s/%s_high_band_receivers_timeplot_%.0dMHz.png' %
                                               (inspectionPlotDir, basename, freqH[fIdx]/1e6))
    fancyPlot(dataL, recL, freqL, dataH, recH, freqH, time,
              inspectionPlotDir='inspection_plots', figFile='%s/%s_both_bands_3d.png' %
                                               (inspectionPlotDir, basename))

    
def inspectionPlotsFromSubbandStatisticsFile(file, keyL='L', keyH='H', plotFreqs=147e6, inspectionPlotDir='inspection_plots'):
    '''
    Make inspection plots of subband statistics file
    
    Input arguments:
    file = string to subband statistics file
    plotFreqs = frequency bin to be plotted (for plots over time for given frequency). Default: 147e6 Hz
    inspectionPlotsDir = string of folder where to place figures
    
    Output arguments:
    none
    '''
    # make sure folder exists
    plib.create_folder(inspectionPlotDir)
    # some conversions
    if not isinstance(plotFreqs, list): # make sure it is a list
        plotFreqs = [plotFreqs]
    band_names, subband_indices = plib.get_band_name_and_subband_index_for_frequency(plotFreqs)
    plotFreqIdx = subband_indices.tolist()
    basename = file.rsplit('/', 1)[-1].split('.')[0]
    (dataL, recL, freqL), (dataH, recH, freqH), time = getDataFromFile(file, keyL=keyL, keyH=keyH)
    
    plotTimeAxis(time, label='Time (UTC)',
                      figFile='%s/%s_timeAxis.png' % (inspectionPlotDir, basename),
                      subtitle=basename)
    plotFreqAxes([freqL, freqH],
                      figFile='%s/%s_freqAxis.png' % (inspectionPlotDir, basename),
                      subtitle=basename)
    plotSubbandStatistics(dataL, time, recL, freqL,
                                         figFile='%s/%s_low_band_receivers_spectra.png' % (inspectionPlotDir, basename))
    plotSubbandStatistics(dataH, time, recH, freqH,
                                         figFile='%s/%s_high_band_receivers_spectra.png' % (inspectionPlotDir, basename))
    plotSSTsForBothBands(dataL, recL, freqL, dataH, recH, freqH, time,
                                         figFile='%s/%s_both_bands_spectra.png' % (inspectionPlotDir, basename))
    waterfallPlotsFromSubbandStatistics(dataL,time, recL, freqL,
                                         subtitle=['Low Band Receivers', basename],
                                         figFile='%s/%s_low_band_receivers_waterfall.png' % (inspectionPlotDir, basename))
    for fIdx in plotFreqIdx:
        waterfallPlotsFromSubbandStatistics(dataH, time, recH, freqH,
                                             subtitle=['High Band Receivers', basename],
                                             figFile='%s/%s_high_band_receivers_waterfall.png' % (inspectionPlotDir, basename))
        timePlotsFromSubbandStatistics(dataL,time, recL, freqL, freqIdx=fIdx,
                                             subtitle=['Low Band Receivers, Frequency %.2f MHz' % (freqL[fIdx]/1e6), 
                                                       basename],
                                             figFile='%s/%s_low_band_receivers_timeplot_%.0dMHz.png' %
                                               (inspectionPlotDir, basename, freqL[fIdx]/1e6))
        timePlotsFromSubbandStatistics(dataH, time, recH, freqH, freqIdx=fIdx,
                                             subtitle=['High Band Receivers, Frequency %.2f MHz' % (freqH[fIdx]/1e6),
                                                       basename],
                                             figFile='%s/%s_high_band_receivers_timeplot_%.0dMHz.png' %
                                               (inspectionPlotDir, basename, freqH[fIdx]/1e6))
    fancyPlot(dataL, recL, freqL, dataH, recH, freqH, time,
              inspectionPlotDir='inspection_plots', figFile='%s/%s_both_bands_3d.png' %
                                               (inspectionPlotDir, basename))
    
    
def waterfallPlotsFromSubbandStatistics(data, time, rec, freq, vmin=50, vmax=110, subtitle='', transpose=True, figFile=None):
    '''
    
    Input arguments:
    data    = data as returned by loadSSTh5
    time    = time as returned by loadSSTh5
    rec     = rec as returned by loadSSTh5
    freq    = freq as returned by loadSSTh5
    recIdx  = recevier index for which the plot should be made
    vmin    = argument as used by pcolor (default: 50 dB)
    vmax    = argument as used by pcolor (default: 110 dB)
    subtitle= string for subtitle of plot (default: '')
              or: list of strings for a multiline subtitle
    transpose = as used by plotWaterfall (default: True)
    
    Output arguments:
    none
    '''
    if not isinstance(subtitle, list):
        subtitle = [subtitle]
    # separate polarisations
    (dataX, recX, dataY, recY) = splitDataAlongPolarisations(data, rec)
    nrow = max([len(recX), len(recY)])
    fig, axs = plt.subplots(nrow, 2, figsize=(12, 2*nrow))
    # plot X receivers
    for recIdx, thisRec in enumerate(recX):
        plotWaterfall(dataX, time, recX, freq, recIdx=recIdx, ax=axs[recIdx][0], vmin=vmin, vmax=vmax, transpose=transpose)
        axs[recIdx][0].set_title(recX[recIdx], y=0.5, color='white', ha='center', va='center')
        if recIdx == np.floor(len(recX)/2): # middle subplot has y label
            axs[recIdx][0].set_xlabel('')
            axs[recIdx][0].set_xticklabels([])
        elif recIdx == len(recX) - 1: # bottom subplot has x label
            axs[recIdx][0].set_ylabel('')
        else:
            axs[recIdx][0].set_xlabel('')
            axs[recIdx][0].set_ylabel('')
            axs[recIdx][0].set_xticklabels([])
    
    # plot Y receivers
    for recIdx, thisRec in enumerate(recY):
        plotWaterfall(dataY, time, recY, freq, recIdx=recIdx, ax=axs[recIdx][1], vmin=vmin, vmax=vmax, transpose=transpose)
        axs[recIdx][1].set_title(recY[recIdx], y=0.5, color='white', ha='center', va='center')
        if recIdx == len(recY) - 1: # bottom subplot has x label
            axs[recIdx][1].set_ylabel('')
            axs[recIdx][1].set_yticklabels([])
        else:
            axs[recIdx][1].set_xlabel('')
            axs[recIdx][1].set_ylabel('')
            axs[recIdx][1].set_xticklabels([])
            axs[recIdx][1].set_yticklabels([])
    fig.subplots_adjust(wspace=0.1)
    fig.subplots_adjust(hspace=0.1)
    fig.suptitle('Receiver Inspection Plot\n%s' % ('\n'.join(line for line in subtitle)))
    if figFile:
        plt.savefig(figFile)
    plt.show()
    
    
def timePlotsFromSubbandStatistics(data, time, rec, freq, freqIdx=201, ymin=50, ymax=110, subtitle='', transpose=True, figFile=None):
    '''
    
    
    Input arguments:
    data    = data as returned by loadSSTh5
    time    = time as returned by loadSSTh5
    rec     = rec as returned by loadSSTh5
    freq    = freq as returned by loadSSTh5
    recIdx  = receiver index for which the plot should be made
    ymin    = minimum y axis value (default: 50 dB)
    ymax    = maximum y axis value (default: 110 dB)
    subtitle= string for subtitle of plot (default: '')
              or: list of strings for a multiline subtitle
    transpose = as used by plotWaterfall (default: True)
    
    Output arguments:
    none
    '''
    if not isinstance(subtitle, list):
        subtitle = [subtitle]
    # separate polarisations
    (dataX, recX, dataY, recY) = splitDataAlongPolarisations(data, rec)
    fig, axs = plt.subplots(2, 1, figsize=(12, 8))
    # X pol
    plotSubbandStatisticsOverTime(dataX, time, recX, freq, freqIdx=freqIdx,
                                       subtitle='Low Band Receivers, X Polarisation',
                                       ymin=ymin, ymax=ymax, ax=axs[0])
    # Y pol
    plotSubbandStatisticsOverTime(dataY, time, recY, freq, freqIdx=freqIdx,
                                       subtitle='Low Band Receivers, Y Polarisation',
                                       ymin=ymin, ymax=ymax, ax=axs[1])
    # fix axes and labels
    axs[0].set_xlabel('')
    axs[0].set_xticklabels([])
    axs[0].set_title('')
    axs[1].set_title('')
    fig.subplots_adjust(wspace=0.1)
    fig.subplots_adjust(hspace=0.1)
    fig.suptitle('Received Power over Time\n%s' % ('\n'.join(line for line in subtitle)))
    if figFile:
        plt.savefig(figFile)
    plt.show()

def plotSubbandStatisticsOverTime(data, time, rec, freq, freqIdx=201, recIdx=None, ax=None, figFile=None, subtitle=None,
                                  ymin=None, ymax=None):
    '''
    Plot the receiver response over time
    
    Input arguments:
    data    = data as returned by loadSSTh5
    time    = time as returned by loadSSTh5
    rec     = rec as returned by loadSSTh5
    freq    = freq as returned by loadSSTh5
    freqIdx = list of frequency index for which the plot should be made (default: 201)
    recIdx  = list of receiver indices for which the plot should be made
              (default: all)
    ax      = ax handle where the plot should be made
              (default: ax of new figure)
    subtitle = string for subtitle on plot. Default: None.
    ymin    = minimum y axis value (default: None)
    ymax    = maximum y axis value (default: None)
    
    Output arguments:
    none
    '''
    if recIdx is None:
        recIdx = [i for i in range(len(data[0]))]
    if not isinstance(recIdx, list): # make list
        recIdx = [recIdx]
    if ax is None:
        fig, ax = plt.subplots(figsize=(12, 6))
    for rIdx in recIdx:
        ax.plot(time, 10*np.log10(data[:,rIdx,freqIdx]), label=rec[rIdx])
    ax.legend()
    ax.grid()
    startDate = '%s-%s-%s' % (time[0].day, time[0].month, time[0].year)
    ax.set_xlabel('Time (start on %s)' % startDate)
    ax.set_ylabel('Power (dB)')
    title = 'Subband Statistics'
    if subtitle:
        title += '\n%s' % subtitle
    ax.set_title(title)
    if ymin or ymax:
        ymin = ymin if ymin is not None else ax.get_ylim()[0]
        ymax = ymax if ymax is not None else ax.get_ylim()[1]
        ax.set_ylim((ymin, ymax))
    if figFile:
        plt.savefig(figFile)

def fancyPlot(dataL, recL, freqL, dataH, recH, freqH, time, xlim=(-1000, 4000), ylim=(-24, 500), zlim=(0, 110), inspectionPlotDir='inspection_plots', recIdx=0, figFile=None):
    
    ax = plt.figure(figsize=(18,12)).add_subplot(projection='3d')
    
    x = np.array([(t - time[0]).seconds for t in time])
    y = np.concatenate((freqL, freqH), axis=0)/1e6
    X, Y = np.meshgrid(x, y, indexing='ij')
    Z = 10*np.log10(np.concatenate((dataL[:,recIdx,:], dataH[:,recIdx,:]), axis=1))
    
    ax.plot_surface(X, Y, Z, rstride=8, cstride=8, alpha=0.3)
    ax.contourf(X, Y, Z, zdir='z', offset=0, cmap=cm.coolwarm)
    ax.contourf(X, Y, Z, zdir='x', offset=-1000, cmap=cm.coolwarm)
    ax.contourf(X, Y, Z, zdir='y', offset=500, cmap=cm.coolwarm)
    
    startDate = '%s-%s-%s' % (time[0].day, time[0].month, time[0].year)
    ax.set(xlim=xlim, ylim=ylim, zlim=zlim,
           xlabel='Time (seconds after %s)' % startDate, ylabel='Frequency (MHz)', zlabel='Power (dB)')
    
    ax.view_init(45, 315)
    
    if figFile:
        plt.savefig(figFile)
        plt.show()

def main():
    return False

if __name__ == '__main__':
    #from sys import exit
    exit(main())