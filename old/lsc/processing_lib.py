# processing_lib.py: Module with generic functions for processing 
# LOFAR2 station data
#
# -*- coding: utf-8 -*-
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.


"""  module processing_lib

This module contains generic functions for processing LOFAR2 station data.

"""
__version__ = '0.1'

import h5py
from os import listdir
from os.path import isfile,join
import os
import datetime
import numpy as np
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib import cm

cst_n_sub = 512 # number of subbands as output of subband generation in firmware
cst_fs = 100e6  # sampling frequency in Hz
bandlims={'LB':[0, 100e6], 'HB1':[100e6, 200e6], 'HB3':[200e6, 300e6]}

def get_timestamp():
    return datetime.datetime.isoformat(datetime.datetime.now())

def create_folder(folders):
    '''
    Create a folder if it not yet exists.
    
    Input arguments:
    folders = string, path to a folder
              or:
              list of strings, multiple paths to a folder
              
    Output arguments:
    none
    '''
    # make sure it is a list
    if not isinstance(folders, list):
        folders = [folders]
    # create folders
    for folder in folders:
        if not os.path.exists(folder):
            os.makedirs(folder)

def get_valid_band_names():
    band_names = []
    for band_name in bandlims:
        band_names.append(band_name)
    return band_names

def get_band_name_and_subband_index_for_frequency(freqs):
    '''
    Get band name and subband index for one or more frequencies.
    
    Input arguments:
    freqs = one or more frequencies in Hz
    bandlims = dict with band names and their limits (default: LOFAR2 bands)
    
    Output arguments:
    band_names = list of band names (see get_valid_band_names)
    sbi        = one or more indices of the subbands for which the frequencies should be returned
    
    '''
    # make sure freqs is a list
    if not isinstance(freqs, list):
        freqs = [freqs]
    # get band name and subband index
    freqs = np.array(freqs)
    band_names = []
    for freq in freqs:
        band_name = get_band_name_for_frequency(freq)
        band_names.append(band_name)
    # get indices for band_name 'LB'
    sbis = np.round(freqs*cst_n_sub/cst_fs)
    # and update for the other bands:
    for idx in np.where(np.array(band_names) == 'HB1')[0]:
        sbis[idx] = 2*cst_n_sub - sbis[idx]
    for idx in np.where(np.array(band_names) == 'HB3')[0]:
        sbis[idx] = sbis[idx] - 2*cst_n_sub
    return band_names, sbis

def get_band_name_for_frequency(freq, bandlims=bandlims):
    '''
    Get band name for frequency
    
    Input arguments:
    freq = one frequency in Hz, float
    bandlims = dict with band names and their limits (default: LOFAR2 bands)
    
    Output arguments:
    band_name = name of analog band (see get_valid_band_names),
                empty string ('') if unsuccessful
    '''
    for band_name in bandlims:
        if np.min(bandlims[band_name]) <= freq and freq <= np.max(bandlims[band_name]):
            return band_name
    return ''

def get_frequency_for_band_name(band_name):
    '''
    Get all frequencies for given band name
    
    Input arguments:
    band_name = name of analog band (see get_valid_band_names)
    
    Output arguments:
    freqs = frequencies in Hz for the selected band
    '''
    return get_frequency_for_band_name_and_subband_index(band_name, sbi=np.arange(512))

def get_frequency_for_band_name_and_subband_index(band_name, sbi):
    '''
    Get frequency for band name and subband(s)
    
    Input arguments:
    band_name = name of analog band (see get_valid_band_names)
    sbi       = one or more indices of the subbands for which the frequencies should be returned
    
    Output arguments:
    freqs = frequencies in Hz for the selected band and subband
    '''
    # check input
    valid_band_names = get_valid_band_names()
    if band_name not in valid_band_names:
        raise Exception('Unknown band_name "%s". Valid band_names are: %s' % (band_name, valid_band_names))
    # generate frequencies
    freqs = sbi*cst_fs/cst_n_sub
    if band_name == 'LB':
        return freqs
    elif band_name == 'HB1':
        return 200e6 - freqs
    elif band_name == 'HB3':
        return 200e6 + freqs

# some basic test methods, for internal use:
def test_get_band_name_and_subband_index_for_frequency():
    input_frequencies = [138.1e6, 137.9e6, 0.1, 0, 195312.5, 234e6]
    expected_band_names = ['HB1', 'HB1', 'LB', 'LB', 'LB', 'HB3']
    expected_subband_index = np.array([317., 318., 0., 0., 1., 174.])
    actual_band_names, actual_subband_index = get_band_name_and_subband_index_for_frequency(input_frequencies)
    if not expected_band_names == actual_band_names:
        print(expected_band_names)
        print(actual_band_names)
        raise Exception('Tests fails!')
    if not (expected_subband_index == actual_subband_index).all():
        print(expected_subband_index)
        print(actual_subband_index)
        raise Exception('Tests fails!')

def test_get_frequency_for_band_name_and_subband_index():
    for input_band_name, input_subband_index, expected_frequencies in zip(['HB1', 'HB1', 'LB', 'LB', 'LB', 'HB3'],
                                                                         [195., 194., 0., 0., 1., 174.], 
                                                                         [161914062.5, 162109375.0, 0., 0., 195312.5, 233984375]):
        actual_frequencies = get_frequency_for_band_name_and_subband_index(input_band_name, input_subband_index)
        if not expected_frequencies == actual_frequencies:
            print(expected_frequencies)
            print(actual_frequencies)
            raise Exception('Tests fails!')

def test():
    test_get_band_name_and_subband_index_for_frequency()
    test_get_frequency_for_band_name_and_subband_index()

def main():
    return False

if __name__ == '__main__':
    #from sys import exit
    exit(main())