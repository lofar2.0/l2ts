import argparse
import time
import sys

from tangostationcontrol.statistics_writer.receiver import tcp_receiver, file_receiver
from tangostationcontrol.statistics_writer.hdf5_writer import sst_hdf5_writer, parallel_xst_hdf5_writer, bst_hdf5_writer

import logging
logging.basicConfig(level=logging.INFO, format = '%(asctime)s:%(levelname)s: %(message)s')
logger = logging.getLogger("statistics_writer")

def _create_parser():
    """Define the parser"""
    parser = argparse.ArgumentParser(
        description='Converts a stream of statistics packets into HDF5 files.')
    parser.add_argument(
        '-a', '--host', type=str, required=False, help='the host to connect to')
    parser.add_argument(
        '-p', '--port', type=int, default=0,
        help='the port to connect to, or 0 to use default port for the '
             'selected mode (default: %(default)s)')
    parser.add_argument(
        '-f', '--file', type=str, required=False, help='the file to read from')
    parser.add_argument(
        '-m', '--mode', type=str, choices=['SST', 'XST', 'BST'], default='SST',
        help='sets the statistics type to be decoded options (default: '
             '%(default)s)')
    parser.add_argument(
        '-i', '--interval', type=float, default=3600, nargs="?",
        help='The time between creating new files in seconds (default: '
             '%(default)s)')
    parser.add_argument(
        '-o', '--output_dir', type=str, default=".", nargs="?",
        help='specifies the folder to write all the files (default: '
             '%(default)s)')
    parser.add_argument(
        '-v', '--debug', dest='debug', action='store_true', default=False,
        help='increase log output')
    parser.add_argument(
        '-d', '--decimation', type=int, default=1,
        help='Configure the writer to only store one every n samples. Saves '
             'storage space')
    parser.add_argument(
        '-r', '--reconnect', dest='reconnect', action='store_true', default=False,
        help='Set the writer to keep trying to reconnect whenever connection '
             'is lost. (default: %(default)s)')
    return parser

def _create_receiver(filename, host, port):
    """ creates the TCP receiver that is given to the writer """
    if filename:
        return file_receiver(filename)
    elif host and port:
        return tcp_receiver(host, port)
    else:
        logger.fatal("Must provide either a host and port, or a file to receive input from")
        sys.exit(1)

def _create_writer(mode, interval, output_dir, decimation):
    """Create the writer"""
    if mode == "XST":
        return parallel_xst_hdf5_writer(new_file_time_interval=interval, file_location=output_dir, decimation_factor=decimation)
    elif mode == "SST":
        return sst_hdf5_writer(new_file_time_interval=interval, file_location=output_dir, decimation_factor=decimation)
    elif mode == "BST":
        return bst_hdf5_writer(new_file_time_interval=interval, file_location=output_dir, decimation_factor=decimation)
    else:
        logger.fatal(f"Invalid mode: {mode}")
        sys.exit(1)

def _start_loop(receiver, writer, reconnect, filename):
    """Main loop"""
    try:
        while True:
            try:
                packet = receiver.get_packet()
                writer.next_packet(packet)
            except EOFError:
                if reconnect and not filename:
                    logger.warning("Connection lost, attempting to reconnect")
                    while True:
                        try:
                            receiver.reconnect()
                        except Exception as e:
                            logger.warning(f"Could not reconnect: {e.__class__.__name__}: {e}")
                            time.sleep(10)
                        else:
                            break
                    logger.warning("Reconnected! Resuming operations")
                else:
                    logger.info("End of input.")
                    raise SystemExit

    except KeyboardInterrupt:
        # user abort, don't complain
        logger.warning("Received keyboard interrupt. Stopping.")
    finally:
        writer.close_writer()

def main():
    
    parser = _create_parser()

    args = parser.parse_args()

    # argparse arguments
    host = args.host
    port = args.port
    filename = args.file
    output_dir = args.output_dir
    interval = args.interval
    mode = args.mode
    decimation = args.decimation
    debug = args.debug
    reconnect = args.reconnect

    if not filename and not host:
        raise ValueError("Supply either a filename (--file) or a hostname (--host)")

    if decimation < 1:
        raise ValueError("Please use an integer --Decimation value 1 or higher to only store one every n statistics' ")

    if port == 0:
        default_ports = { "SST": 5101, "XST": 5102, "BST": 5103 }
        port = default_ports[mode]

    if debug:
        logger.setLevel(logging.DEBUG)
        logger.debug("Setting loglevel to DEBUG")

    # creates the TCP receiver that is given to the writer
    receiver = _create_receiver(filename, host, port)

    # create the writer
    writer = _create_writer(mode, interval, output_dir, decimation)

    # start looping
    _start_loop(receiver, writer, reconnect, filename)

main()
