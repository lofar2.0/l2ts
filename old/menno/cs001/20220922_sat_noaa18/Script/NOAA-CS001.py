# beams on NOAA satelitte
# M.J. Norden
# Version 0.1       25-nov-2011
# Version 0.2       16-sep-2022
# NOAA-19 subband 190 137.109 MHz
# NOAA-15 subband 193 137.695 MHz
# NOAA-18 subband 195 138.085 MHz # today sat
# 25 nov: creation script

import sys
from optparse import OptionParser
from mmap import mmap 
import time
import array
import os
import commands
import operator
import math
import numpy
import string

########################################################################################
# Init

# Variables
########################################################################################
# Function DisableElements
# 

def DisableElements(tile,elem):
         
	time.sleep(3)
	rcu=tile*2
	res = os.popen3('rspctl --realdelays --sel=%i' % (rcu+1) )[1].readlines() 
	a = str.split(res[1])
	
	
	# for Tile number > 5
	if len(a) < 19 :
	     b = a[2:]
	     for pos in elem:
	         b[pos-1]='2' # switch off element (no DC)	
	else: # and < 5
	     b = a[3:]
	     for pos in elem:
	         b[pos-1]='2' # switch off element (no DC)	     	
			
	os.system('rspctl --hbadelays=%s --sel=%i,%i' % (','.join(b),rcu,rcu+1))
	
	return

########################################################################################
# Function DisableHBAupdate to disable HBAupdateInterval
# 

def DisableHBAupdate(): 
	   
	res = os.popen3("sed -i 's/BeamServer.HBAUpdateInterval= 180/BeamServer.HBAUpdateInterval= 180000/g' /opt/lofar/etc/BeamServer.conf")[1].readlines() 
	print "Disable HBAUpdateInterval"
	   
	return

########################################################################################
# Function EnableHBAupdate to enable HBAupdateInterval
# 

def EnableHBAupdate(): 
	   
	res = os.popen3("sed -i 's/BeamServer.HBAUpdateInterval= 180000/BeamServer.HBAUpdateInterval= 180/g' /opt/lofar/etc/BeamServer.conf")[1].readlines() 
	print "Enable HBAUpdateInterval"   
	   
	return

########################################################################################
# Function SetRingSplitter to split HBA ears
# 

def SetRingSplitter():

        res = os.popen3('rspctl --splitter=1')[1].readlines() 
	print "Set Ring Splitter on"
	
        return

########################################################################################
# Function CheckSwlevel
# To check that the beamserver is running before beams are started

def CheckSwlevel():
         
	res = os.popen3('swlevel -S')[1].readlines() 
	a = int(res[0])

        if a < 6:
           os.system('swlevel 6')
	   print "Software level is 6"   
        else:
           print "Software level unchanged"
        return

########################################################################################
# Function TurnOn48V
# To check that the beamserver is running before beams are started

def TurnOn48():
         
	res = os.popen3('isTurnOn48V.py')[1].readlines() 
	print "Switch on +48V power supply"
	
        return


########################################################################################
# Function TurnOff48V
# To check that the beamserver is running before beams are started

def TurnOff48():
         
	res = os.popen3('isTurnOff48V.py')[1].readlines() 
	print "Switch off +48V power supply"
	
        return

########################################################################################
# Function Swlevel2
# To check that the beamserver is running before beams are started

def Swlevel2():
         
	res = os.popen3('swlevel 2')[1].readlines() 
	print "Software goes into Swlevel 2"
	time.sleep(10)
        return

########################################################################################
# Function Sleep Until
# Sleep unit date (yyyymmdd) and time (hhmmss;UTC)
				
def SleepUntil(date_slp,time_slp):
	
	localtime = time.asctime( time.localtime(time.time()) )
        print "Local current time :", localtime
        os.system('/globalhome/lofarsys/bin/sleepuntil.sh %s %s' %(date_slp,time_slp))
        return


########################################################################################
# Function CalcAzmElv
# Calculate Azimuth (Azm) and Elevation (Elv) of NOAA sat in radians
				
def CalcAzmElv(a,b):
	
	global Azm,Elv
	
	Azm=(2*math.pi/360)*a
	Elv=(math.pi/180)*b
			
        return

########################################################################################
# Function BeamStartup
# Sleep unit date (yyyymmdd) and time (hhmmss;UTC)
				
def BeamStartup(array,rcu,band,sb,bl,az,el):
	
	time.sleep(3)
	direc = str("%.3f,%.3f,AZELGEO" % (az,el))
	os.system('beamctl --antennaset=%s --rcus=%s --band=%s --subbands=%s --beamlets=%s --anadir=%s --digdir=%s &' %(array,rcu,band,sb,bl,direc,direc))
        time.sleep(3)
	return

########################################################################################
# Function Create Datadirectory
# 
				
def CreateDataDir():
	
	global datadir
	
	datadir='/localhome/data/sat'
	
	os.system('rm -r %s' % datadir)
	os.system('mkdir %s' % datadir)
	
	return

########################################################################################
# Function WriteData
# 
				
def WriteData():
	
	os.system('rspctl --stati=beamlet --int=1 --dur=1200 --directory=%s' % datadir)
        
	return

########################################################################################
# Function KillBeams
# Stop all beamctl processes
				
def KillBeams():
	
	os.system('killall beamctl')
        
	return


########################################################################################
# Main program
#
# This program calculates the maximum position of a NOAA satellite. This position
# is in Azimuth and Elevation. The calculated position is used to point a HBA beam.

# this is the prepare time. Minimum 300 seconds before record
SleepUntil('20220922','224000') # date, time (UTC)
Swlevel2()
#DisableHBAupdate()
CreateDataDir()
CalcAzmElv(109.3,86.3) # Azimuth, Elevation (degrees) output radians
KillBeams()
CheckSwlevel()
#SetRingSplitter()


# HBA ZERO splitter=0
BeamStartup('HBA_ZERO','0:1','110_190','190:195','0:5',Azm,Elv)
BeamStartup('HBA_ZERO','6:7','110_190','190:195','6:11',Azm,Elv)
BeamStartup('HBA_ZERO','8:9','110_190','190:195','12:17',Azm,Elv)
BeamStartup('HBA_ZERO','10:11','110_190','190:195','18:23',Azm,Elv)
BeamStartup('HBA_ZERO','16:17','110_190','190:195','24:29',Azm,Elv)
BeamStartup('HBA_ZERO','18:19','110_190','190:195','30:35',Azm,Elv)
BeamStartup('HBA_ZERO','20:21','110_190','190:195','36:41',Azm,Elv)
BeamStartup('HBA_ZERO','28:29','110_190','190:195','42:47',Azm,Elv)
BeamStartup('HBA_ZERO','30:31','110_190','190:195','48:53',Azm,Elv)
BeamStartup('HBA_ZERO','16:31','110_190','190:195','54:59',Azm,Elv)
BeamStartup('HBA_ZERO','0:47','110_190','190:195','60:65',Azm,Elv)
BeamStartup('HBA_ZERO','32:33','110_190','190:195','66:71',Azm,Elv)
BeamStartup('HBA_ZERO','6:11,16:21,28:33','110_190','190:195','72:77',Azm,Elv)
BeamStartup('HBA_ONE','48:95','110_190','190:195','78:83',Azm,Elv)
BeamStartup('HBA_ONE','64:65','110_190','190:195','84:89',Azm,Elv)
BeamStartup('HBA_ONE','66:67','110_190','190:195','90:95',Azm,Elv)
BeamStartup('HBA_ONE','76:77','110_190','190:195','96:101',Azm,Elv)
BeamStartup('HBA_ONE','78:79','110_190','190:195','102:107',Azm,Elv)
BeamStartup('HBA_ONE','64:79','110_190','190:195','108:113',Azm,Elv)

# wait for the start of the recording. Should be 308 sec before maximum!
SleepUntil('20220922','224805') # date, time (UTC) - 10 minutes and 8 seconds
WriteData()
#EnableHBAupdate()
KillBeams()
########################################################################################




