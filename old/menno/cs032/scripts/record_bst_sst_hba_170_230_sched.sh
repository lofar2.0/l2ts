#!/bin/bash
#
# Script to record BST and SST data for CS032 (LOFAR1) and CS001 (LOFAR2) coparison
# version 1.0  1-12-2022, M.J.Norden
# version 1.1  19-12-2022 for mode 7 210_250MHz
# version 1.2  2-2-2023 for mode 6 170_230 MHz (160 MHz clock)

#---------------------------------------------------

function sleepuntil {
## until given time
        local date_num=$1
        local time_num=$2

        while true; do
                if test `date +"%Y%m%d%H%M%S"` -ge ${date_num}${time_num}; then
                        break;
                fi
                sleep 1
        done
}


####################################################
# Main script
####################################################

## arguments:
# 1 date (YYYYMMDD)
# 2 time (HHMMSS)


if    [ $# -lt 2 ]; then
        echo "Usage:  <YYYYMMDD> <HHMMSS>"
        echo "or"
        echo "nohup lcurun -s cs032c -c '/opt/stationtest/test/beamformertest/record_bst_sst_hba_170_230_sched.sh'<YYYYMMDD> <HHMMSS> &"
        exit;
fi


YYYYMMDD=$1
HHMMSS=$2


## Execute sleepuntil:
if [[ $YYYYMMDD != "" && $HHMMSS != "" ]]; then

        sleepuntil $YYYYMMDD $HHMMSS
else
        echo "INFO# no sleep until defined, start right away" >> $script_log
fi

#########################################################
#start script
###########################################################
#set station in correct initial mode
swlevel 2
sleep 3

# set station to 160MHz clock
rspctl --clock=160
sleep 60

rspctl --wg=0
sleep 1
rspctl --rcuprsg=0
sleep 1
rspctl --bitmode=8
sleep 2

# Set splitter in ON state
rspctl --splitter=1
sleep 2

# 2022-11-01 JS Test
# If the ClockControl is active, it is not possible to switch the splitter from its state
# The splitter should be OFF :
# if latest observation was HBA the splitter could not be set to off
# if latest observation was LBA the splitter was already off
# therefore we always stop the ClockControl to switch the splitter
# go to swlevel 5 to kill the ClockControl, so the splitter can be switched off
swlevel 3
sleep 2

# variables

#nrcus
#let rspboards=`sed -n  's/^\s*RS\.N_RSPBOARDS\s*=\s*\([0-9][0-9]*\).*$/\1/p' /opt/lofar/etc/RemoteStation.conf`
#let nrcus=8*$rspboards-1

#duration of the recording (13hr * 3600 sec)
let duration=46800

#current date and time
let datetime=`date +"%Y%m%d%H%M%S"`

# create a beam to setup HBA mode (only correct setup rcumode is important)
beamctl --antennaset=HBA_ZERO --band=170_230 --rcus=0:47 --subbands=0:487 --beamlets=0:487 --anadir=0,1.5708,AZELGEO --digdir=0,1.5708,AZELGEO&
sleep 10
# create a beam to setup HBA mode (only correct setup rcumode is important)
beamctl --antennaset=HBA_ONE --band=170_230 --rcus=48:95 --subbands=0:487 --beamlets=1000:1487 --anadir=0,1.5708,AZELGEO --digdir=0,1.5708,AZELGEO&
sleep 10

# recording BST data for HBA_ZERO 
mkdir /localhome/data/${datetime}_BST_HBA_ZERO
sleep 2
rspctl --statistics=beamlet --integration 1 --duration $duration --directory /localhome/data/${datetime}_BST_HBA_ZERO &

# recording SST data for HBA_ZERO parallel with BST data
# create first the directory
mkdir /localhome/data/${datetime}_SST_HBA_ZERO
sleep 2
rspctl --statistics --integration=1 --duration=$duration --sel=0:47 --directory=/localhome/data/${datetime}_SST_HBA_ZERO &

# recording BST data for HBA_ONE
mkdir /localhome/data/${datetime}_BST_HBA_ONE
sleep 2
rspctl --statistics=beamlet --integration 1 --duration $duration --directory /localhome/data/${datetime}_BST_HBA_ONE &

# recording SST data for HBA_ONE parallel with BST data
# create first the directory
mkdir /localhome/data/${datetime}_SST_HBA_ONE
sleep 2
rspctl --statistics --integration=1 --duration=$duration --sel=48:95 --directory=/localhome/data/${datetime}_SST_HBA_ONE &

# Sleep until the observation is ready
sleepuntil 20230203 054000

# to be sure no beams are running
killall beamctl
rspctl --mode=0
# switch back the clock to 200MHz (default)
rspctl --clock=200
sleep 60

# go to swlevel 6 to restart the ClockControl and others
swlevel 6

