import numpy as np
import matplotlib.pyplot as plt
GainADC=(10*np.log10(100e6)+20*np.log10(2**12)) #XST to dBFS

def loadXST(fn0,Iexp=range(512),Ni=12,Npar=7,indexfix=True):
    cnt=0;
    Data=np.load(fn0+".npz")
    Bands=Data['Bands']
    Data=Data['Data']
    Bands2=np.zeros_like(Bands)
    #Fix wrong indexes
    if indexfix:
      Bands=(Bands-7)%512
      Bands[:-1]=(Bands[:-1]-1*(Bands[:-1]==Bands[1:]))%512
#    for c,i1 in enumerate(Bands):
#         i1=(np.where(Iexp==i1)[0][0]-1-Npar)%len(Iexp)
#         if (c>0) and ((Bands2[-1]+2)%len(Iexp)==i1): i1=(i1-1)%len(Iexp)
#         Bands2[c]=i1;

#    print(Bands[:len(Bands2)//Npar*Npar].reshape([len(Bands2)//Npar,Npar])[:])
#    print(Bands2[:len(Bands2)//Npar*Npar].reshape([len(Bands2)//Npar,Npar])[:3])
#    print(P2dB(D2[:,3,3].real.reshape([len(Idx)//7,7]))-GainADC)
    #Ids,indices=np.unique(Idx,return_inverse=True)
    Nband=len(Iexp)
    Dcnt=np.zeros([Nband],dtype='int')
    Dsum=np.zeros([Nband,Ni,Ni],dtype='complex')
    for i,b in enumerate(Bands):
        Dsum[b]+=Data[i,:,:]
        Dcnt[b]+=1;
 
#    Ids2=[Iexp[x] for x in Ids]
    return Dsum,Dcnt


def loadXSTs(fn0,Fcnt,Iexp=range(512),Ni=12,Npar=7,indexfix=True):
    Dsum,Dcnt=loadXST(fn0+"_0",Iexp=Iexp,Ni=Ni,Npar=Npar,indexfix=indexfix)
    for x in range(1,Fcnt):
        Dsum2,Dcnt2=loadXST(fn0+"_%i"%x,Iexp=Iexp,Ni=Ni,Npar=Npar)
        Dsum+=Dsum2;
        Dcnt+=Dcnt2;
    return (Dsum.T/Dcnt).T,Dcnt
def P2dB(P): return 10*np.log10(P+1e-3);
def XSTfrequency(Ids=range(512)):
    return np.array(Ids,dtype='float')/512.*100
def GetPwr(Dsum,makeplot=False,Cut=None):
    Pi=[]
    for i in range(Dsum.shape[1]):
      Pi.append(np.sum(np.abs(Dsum[2:,i,i].real)))
    Pi=np.array(Pi)
    if makeplot:
        plt.plot(P2dB(Pi)-GainADC,'k.')
        plt.xlabel("SI #")
        plt.ylabel("Total power (dB)")
        plt.grid()
        plt.title("Power in each channel") 
    if not(Cut is None):
        Chs=np.compress(P2dB(Pi)-GainADC>Cut,range(len(Pi)))
        print("Active channels:", Chs)
        return Pi,Chs
    return Pi
def PlotSST(freq,Dsum,savefig=None,Chs=None):
        if Chs is None: Chs=range(Dsum.shape[1])
        plt.figure(dpi=300)
        for i in Chs:
          plt.plot(freq,P2dB((Dsum[:,i,i].real))-GainADC,label=str(i),linewidth=0.5)
        plt.xlabel("Frequency (MHz)")
        plt.ylabel("Power (dBFS)")
        plt.title("Spectrum of each channel")
        plt.legend(ncol=2,fontsize=8)
#        plt.xlim(0,100)
#        plt.ylim(50,60)
        if not(savefig is None): plt.savefig(savefig+'_diag.png')
        
def Makepairs(Chs):
    return [[i,j] for a,i in enumerate(Chs) for j in Chs[a+1:] ]

def MakeRCUpairs(RCUs):
    pairs=[[i+r*3,j+r*3] for r in RCUs for i,j in [[0,1],[0,2],[1,2]] ]
    for rd in range(1,1+len(RCUs)):
        pairs+=[[i+RCUs[r1]*3,j+RCUs[r1+rd]*3] for r1 in range(len(RCUs)-rd) for i in range(3) for j in range(3)]
#    for ri,r1 in enumerate(RCUs) for r2 in RCUs[ri:] for k in range(3) for j in range(3):
    return pairs

def Pairs_sharing_FFT(pairs):
    pairs1=[] #Pairs sharing FFT
    for i,j in pairs:
      if (j==i+1) and (i%2==0): 
          pairs1.append([i,j])
    return pairs1;
def Pairs_not_sharing_FFT(pairs):
    pairs1=[] #Pairs sharing FFT
    for i,j in pairs:
      if not((j==i+1) and (i%2==0)): 
          pairs1.append([i,j])
    return pairs1;

def XSTpower(pairs,Dsum,makeplot=False,cut=None):
    Pp=[]
    for i,j in pairs:
      Pp.append(np.sum(np.abs(Dsum[2:,i,j])))
    Pp=10*np.log10(np.array(Pp))-GainADC;
    if makeplot:
        plt.figure(dpi=300)
        if not(cut is None): plt.plot([0,len(Pp)],[cut,cut],'r-')
        plt.plot(Pp)
        plt.title("sum(abs(XST)) of pairs")
        plt.ylabel("(dBFS)")
        plt.xlabel("Signal input pairs")
        plt.xticks(range(len(pairs)), ["%i-%i" % (i,j) for i,j in pairs],rotation=90,fontsize=7);
    if not(cut is None):
        pairs2=[]
        pairs3=[]
        for i,p in enumerate(Pp):
          if p>cut:
                pairs3.append(pairs[i])
          else:
                pairs2.append(pairs[i])
        return Pp,pairs2,pairs3
    return Pp
def XSTreal(pairs,Dsum,makeplot=False,cut=None):
    Pp=[]
    for i,j in pairs:
      Pp.append(np.sum(np.real(Dsum[2:,i,j])))
    Pp=10*np.log10(np.abs(np.array(Pp)))-GainADC;
    if makeplot:
        plt.figure(dpi=300)
        if not(cut is None): plt.plot([0,len(Pp)],[cut,cut],'r-')
        plt.plot(Pp,'.')
        plt.xlim([-0.5,len(Pp)-0.5])
        plt.title("sum(real(XST)) of pairs")
        plt.ylabel("Power (dBFS)")
        plt.xlabel("Signal input pairs")
        plt.xticks(range(len(pairs)), ["%i-%i" % (i,j) for i,j in pairs],rotation=90,fontsize=7);
    if not(cut is None):
        pairs2=[]
        pairs3=[]
        for i,p in enumerate(Pp):
          if p>cut:
                pairs3.append(pairs[i])
          else:
                pairs2.append(pairs[i])
        return Pp,pairs2,pairs3
    return Pp
def NoiseSuppression(cnt,Tint=1):
 return P2dB(200e6/1024*Tint*cnt)/2;

def plotXSTpairs(freq,pairs,Dsum,plotsum=True,NoiseSuppress=0,linewidth=0.1): #NoiseSuppress>0 then plot SSTs of first 2
  Sum=[]
  for i,j in pairs:
    S=Dsum[1:,i,j]
    plt.plot(freq[1:],P2dB(np.abs(S))-GainADC,label=str(i)+'-'+str(j),linewidth=linewidth)
    Sum.append(S);
  if (plotsum) and len(pairs)>0:
    Sum=np.mean(np.array(Sum).real,axis=0)
    plt.plot(freq[1:],P2dB(np.abs(Sum))-GainADC,'k-',label='sum',linewidth=0.5)
  if NoiseSuppress>0:
     for i in pairs[0]:
       plt.plot(freq[1:],P2dB(np.abs(Dsum[1:,i,i]))-GainADC-NoiseSuppress,'r',linewidth=1)
       plt.plot(freq[1:],P2dB(np.abs(Dsum[1:,i,i]))-GainADC-NoiseSuppress-P2dB(len(pairs))/2,'r:',linewidth=1)
     plt.plot([],[],'r-',label='Expected')

  plt.xlabel('Frequency (MHz)')
  plt.ylabel('Power (dBFS)')
  plt.legend(fontsize=4,ncol=4)
  plt.title("XST power")

def plotFFTXST(pairs,Dsum,roll=False):
    plt.figure(dpi=300)
    for c,p in enumerate(pairs):
        D=Dsum[:,p[0],p[1]]
        D[0]=0
        A=np.fft.fft(D,norm='ortho')
        A=np.abs(A)
        N=len(A)
        t=np.arange(N)
        if roll:
            t-=N*(t>N//2)
            t=np.concatenate([t[N//2+1:],t[:N//2+1]])
            A=np.concatenate([A[N//2+1:],A[:N//2+1]])
        plt.plot(t,A,'-',label=str(p[0])+'-'+str(p[1]),linewidth=0.5)
    plt.xlabel('Time Offset')
    plt.ylabel('Magnitude')
    plt.title("fft(XST) of pairs above threshold")
    plt.legend(fontsize=4,ncol=4)

def plotFFTXSTreal(pairs,Dsum,roll=False):
    plt.figure(dpi=300)
    for c,p in enumerate(pairs):
        D=Dsum[:,p[0],p[1]]
        D[0]=0
        A=np.fft.fft(D,norm='ortho').real
        N=len(A)
        t=np.arange(N)
        if roll:
            t-=N*(t>N//2)
            t=np.concatenate([t[N//2+1:],t[:N//2+1]])
            A=np.concatenate([A[N//2+1:],A[:N//2+1]])
        plt.plot(t,A,'-',label=str(p[0])+'-'+str(p[1]),linewidth=0.5)
    plt.xlabel('Time Offset')
    plt.ylabel('Magnitude')
    plt.title("fft(XST) of pairs above threshold")
    plt.legend(fontsize=4,ncol=4)    
def plotXSTphase(freq,pairs,Dsum,markersize=0.5,fontsize=4):
  plt.figure(dpi=300)
  Csum=np.zeros([512],dtype='complex')
  Cntsum=0
  for c,(i,j) in enumerate(pairs):
#    if Pwrs[c]>Pcut: continue
#      C=Dsum[:,i,j]/np.sqrt(Dsum[:,i,i].real*Dsum[:,j,j].real)
    plt.plot(freq,np.angle(Dsum[:,i,j])/np.pi*180,'.',markersize=markersize,label=str(i)+'-'+str(j),linewidth=0.1)
#    Csum+=(Data2[:,i,j])
#    Cntsum+=1;
    plt.legend(ncol=2,fontsize=fontsize,loc='upper right')#Csum/=Cntsum
  plt.xlabel('Frequency (MHz)')
  plt.ylabel('Phase (degrees)')
  plt.ylim(-180,180)
  plt.title("XST angle")

def plotXSTcorr(freq,pairs,Dsum):
  plt.figure(dpi=300)
  Csum=np.zeros([512],dtype='complex')
  Cntsum=0
  for c,(i,j) in enumerate(pairs):
    C=Dsum[:,i,j]/np.sqrt(Dsum[:,i,i].real*Dsum[:,j,j].real)
    plt.plot(freq,100*np.abs(C),'.',markersize=0.5,label=str(i)+'-'+str(j),linewidth=0.1)
#    Csum+=(Data2[:,i,j])
#    Cntsum+=1;
    plt.legend(ncol=2,fontsize=4)#Csum/=Cntsum
  plt.xlabel('Frequency (MHz)')
  plt.ylabel('Correlation (%)')
  plt.ylim(0,100)
#  plt.title("% Correlation")

def plotXSTlevels(freq,pairs,Dsum,ReqBelowSST):
  Sum=[]
  Avg=[]
  for i,j in pairs:
    S=Dsum[1:,i,j]
    Sum.append(S);
    S=P2dB(np.abs(S))-GainADC
    Avg.append(S);
#    plt.plot(freq[1:],S,label=str(i)+'-'+str(j),linewidth=0.1)
  if len(pairs)>0:
    Sum=np.array(Sum)
    Sum2=np.mean(Sum,axis=0)#.real
    Avg=np.array(Avg)
    Avg2=np.mean(Avg,axis=0)
    Std=np.std(Avg,axis=0)
#    plt.plot(freq[1:],P2dB(np.abs(Sum2))-GainADC,'k-',label='XST sum',linewidth=0.5)
    Sum3=np.mean(Sum2[1:511].reshape([102,5]),axis=1)
    plt.plot(freq[2:512:5],P2dB(np.abs(Sum3))-GainADC,'k-',label='Correlated noise (1MHz)',linewidth=0.5)

    Sum3=np.mean(Sum2[1:511].reshape([10,51]),axis=1)
    plt.plot(freq[25::51],P2dB(np.abs(Sum3))-GainADC,'k.',label='Correlated noise (10MHz)',linewidth=0.5)

    plt.plot(freq[1:],Avg2,'b-',label='XST (+- std)',linewidth=1)
    plt.plot(freq[1:],Avg2+Std,'b:',linewidth=0.5)
    plt.plot(freq[1:],Avg2-Std,'b:',linewidth=0.5)
  Sum=[]
  for i,j in pairs:
      Sum.append(P2dB(np.abs(Dsum[1:,i,i]))-GainADC)
      Sum.append(P2dB(np.abs(Dsum[1:,j,j]))-GainADC)
  if len(pairs)>0:
    Sum=np.array(Sum)
    Avg=np.mean(Sum,axis=0)
    Std=np.std(Sum,axis=0)
    plt.plot(freq[1:],Avg,'m-',label='SST',linewidth=1)
    plt.plot(freq[1:],Avg+ReqBelowSST,'r-',label='Spurious requirement\n(SST%.1f)'%ReqBelowSST,linewidth=1)
#    plt.plot(freq[1:],Avg+Std,'m:',linewidth=0.5)
#    plt.plot(freq[1:],Avg-Std,'m:',linewidth=0.5)

  plt.xlabel('Frequency (MHz)')
  plt.ylabel('Power (dBFS)')
  plt.legend(fontsize='small',ncol=1,loc='upper right')
  plt.title("Correlated noise between channels")