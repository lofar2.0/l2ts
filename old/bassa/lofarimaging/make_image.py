#!/usr/bin/env python3
import re
import os
import sys
import datetime

import numpy as np
import matplotlib.pyplot as plt

import astropy.units as u
from astropy.time import Time
from astropy.coordinates import SkyCoord, EarthLocation, AltAz, GCRS, get_sun

from lofarantpos.db import LofarAntennaDatabase
from lofarimaging import get_station_type, get_full_station_name, freq_from_sb, read_acm_cube
from lofarimaging import apply_calibration, get_station_xyz, skycoord_to_lmn, sky_imager
from lofarimaging import make_sky_plot

if __name__ == "__main__":
    xst_filename = sys.argv[1]
    station_name = "CS001"
    caltable_dir = "CalTables"

    rcu_mode = re.search("mode_([^_]*)", xst_filename).groups(0)[0]
    subband = int(re.search("sb([0-9]*)", xst_filename).groups(0)[0])
    obsdatestr, obstimestr, *_ = os.path.basename(xst_filename).rstrip(".dat").split("_")
    station_type = get_station_type(station_name)
    station_name = get_full_station_name(station_name, rcu_mode)

    # Get the data
    fname = f"{obsdatestr}_{obstimestr}_{station_name}_SB{subband:03d}"

    npix_l, npix_m = 131, 131
    freq = freq_from_sb(subband, rcu_mode=rcu_mode)

    # Which slice in time to visualise
    timestep = 0

    obstime = datetime.datetime.strptime(obsdatestr + ":" + obstimestr, '%Y%m%d:%H%M%S')

    # Read cube
    cube = read_acm_cube(xst_filename, station_type)

    cube, calibration_info = apply_calibration(cube, station_name, rcu_mode, subband, caltable_dir=caltable_dir)
    # Split into the XX and YY polarisations (RCUs)
    # This needs to be modified in future for LBA sparse
    cube_xx = cube[:, 0::2, 0::2]
    cube_yy = cube[:, 1::2, 1::2]
    visibilities_all = cube_xx + cube_yy
    
    # Stokes I for specified timestep
    visibilities = visibilities_all[timestep]

    # Setup the database
    db = LofarAntennaDatabase()

    station_xyz, pqr_to_xyz = get_station_xyz(station_name, rcu_mode, db)
    # For every antenna, calculate the distance between it and every other antenna
    baselines = station_xyz[:, np.newaxis, :] - station_xyz[np.newaxis, :, :]

    # Account for the rotation
    rotation = np.rad2deg(db.rotation_from_north(station_name))
    
    # Determine positions of Cas A and Cyg A
    obstime_astropy = Time(obstime)
    station_earthlocation = EarthLocation.from_geocentric(*(db.phase_centres[station_name] * u.m))
    zenith = AltAz(az=0 * u.deg, alt=90 * u.deg, obstime=obstime_astropy,
                   location=station_earthlocation).transform_to(GCRS)
    
    marked_bodies = {
        'Cas A': SkyCoord(ra=350.85*u.deg, dec=58.815*u.deg),
        'Cyg A': SkyCoord(ra=299.868*u.deg, dec=40.734*u.deg),
        #        'Per A': SkyCoord.from_name("Perseus A"),
        #        'Her A': SkyCoord.from_name("Hercules A"),
        #        'Cen A': SkyCoord.from_name("Centaurus A"),
        #        '?': SkyCoord.from_name("J101415.9+105106"),
        #        '3C295': SkyCoord.from_name("3C295"),
        #        'Moon': get_moon(obstime_astropy, location=station_earthlocation).transform_to(GCRS),
        'Sun': get_sun(obstime_astropy)
        #        '3C196': SkyCoord.from_name("3C196")
    }

    marked_bodies_lmn = {}
    for body_name, body_coord in marked_bodies.items():
        #print(body_name, body_coord.separation(zenith), body_coord.separation(zenith))
        if body_coord.transform_to(AltAz(location=station_earthlocation, obstime=obstime_astropy)).alt > 0:
            marked_bodies_lmn[body_name] = skycoord_to_lmn(marked_bodies[body_name], zenith)

    img = sky_imager(visibilities, baselines, freq, npix_l, npix_m)
    fig = make_sky_plot(img, marked_bodies_lmn, title=f"Sky image for {station_name}",
                        subtitle=f"SB {subband:03d} ({freq / 1e6:.1f} MHz), {str(obstime)[:19]}");
    plt.savefig(f"results/{fname}.png", bbox_inches="tight")
