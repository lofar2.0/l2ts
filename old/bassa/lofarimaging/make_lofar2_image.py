#!/usr/bin/env python3
import re
import os
import sys
import h5py
import datetime

import numpy as np
import matplotlib.pyplot as plt

import astropy.units as u
from astropy.time import Time
from astropy.coordinates import SkyCoord, EarthLocation, AltAz, GCRS, get_sun

from lofarantpos.db import LofarAntennaDatabase
from lofarimaging import get_station_type, get_full_station_name, freq_from_sb, read_acm_cube
from lofarimaging import apply_calibration, get_station_xyz, skycoord_to_lmn, sky_imager
from lofarimaging import make_sky_plot

def read_lofar2_xst(fname):
    # Open file
    h5 = h5py.File(fname, "r")

    # Read keys
    keys = [key for key in h5.keys()]

    # Read visibilities
    l2_visibilities = np.array(h5[keys[0]]["values"])

    # Fill missing parts
    for i in range(12, 18):
        for j in range(0, 12):
            l2_visibilities[j, i] = l2_visibilities[i, j]

    # LOFAR 2.0 LBA_OUTER antenna selection (2022-11-17)
    ant_idx = np.array([72, 77, 83, 88, 90, 92, 93, 94, 95])
    ant_idx = np.array([72, 77, 83, 88, 90, 92, 93, 95])

    # Offset
    ant_idx -= 48
    
    # Create LOFAR1 visibility cube
    cube = np.zeros((96, 96)).astype(np.complex128)
    for i in range(len(ant_idx)):
        for j in range(len(ant_idx)):
            cube[2 * ant_idx[i], 2 * ant_idx[j]] = l2_visibilities[2 * i, 2 * j]
            cube[2 * ant_idx[i] + 1, 2 * ant_idx[j] + 1] = l2_visibilities[2 * i + 1, 2 * j + 1]

    return cube


if __name__ == "__main__":
    xst_filename = sys.argv[1]
    station_name = "CS001"
    caltable_dir = "CalTables"

    # Read LOFAR2.0 HDF5 XST and save as LOFAR1 RCU Mode 1
    cube = read_lofar2_xst(xst_filename)
    subband = int(re.search("SB([0-9]*)", xst_filename).groups(0)[0])
    obsdatestr = xst_filename.split("_")[-1].replace(".h5", "").replace("-", "")
    obsdatestr = f"{obsdatestr[:8]}_{obsdatestr[8:]}"
    xst_filename = f"{obsdatestr}_mode_1_xst_sb{subband:03d}.dat"
    cube.tofile(xst_filename)
    
    rcu_mode = re.search("mode_([^_]*)", xst_filename).groups(0)[0]
    subband = int(re.search("sb([0-9]*)", xst_filename).groups(0)[0])
    obsdatestr, obstimestr, *_ = os.path.basename(xst_filename).rstrip(".dat").split("_")
    station_type = get_station_type(station_name)
    station_name = get_full_station_name(station_name, rcu_mode)

    # Get the data
    fname = f"{obsdatestr}_{obstimestr}_{station_name}_SB{subband:03d}"

    npix_l, npix_m = 131, 131
    freq = freq_from_sb(subband, rcu_mode=rcu_mode)

    # Which slice in time to visualise
    timestep = 0

    obstime = datetime.datetime.strptime(obsdatestr + ":" + obstimestr, '%Y%m%d:%H%M%S')

    # Read cube
    cube = read_acm_cube(xst_filename, station_type)

#    cube, calibration_info = apply_calibration(cube, station_name, rcu_mode, subband, caltable_dir=caltable_dir)
    # Split into the XX and YY polarisations (RCUs)
    # This needs to be modified in future for LBA sparse
    cube_xx = cube[:, 0::2, 0::2]
    cube_yy = cube[:, 1::2, 1::2]
    visibilities_all = cube_xx + cube_yy
    
    # Stokes I for specified timestep
    visibilities = visibilities_all[timestep]

    # Setup the database
    db = LofarAntennaDatabase()

    station_xyz, pqr_to_xyz = get_station_xyz(station_name, rcu_mode, db)
    # For every antenna, calculate the distance between it and every other antenna
    baselines = station_xyz[:, np.newaxis, :] - station_xyz[np.newaxis, :, :]

    # Account for the rotation
    rotation = np.rad2deg(db.rotation_from_north(station_name))
    
    # Determine positions of Cas A and Cyg A
    obstime_astropy = Time(obstime)
    station_earthlocation = EarthLocation.from_geocentric(*(db.phase_centres[station_name] * u.m))
    zenith = AltAz(az=0 * u.deg, alt=90 * u.deg, obstime=obstime_astropy,
                   location=station_earthlocation).transform_to(GCRS)
    
    marked_bodies = {
        'Cas A': SkyCoord(ra=350.85*u.deg, dec=58.815*u.deg),
        'Cyg A': SkyCoord(ra=299.868*u.deg, dec=40.734*u.deg),
        #        'Per A': SkyCoord.from_name("Perseus A"),
        #        'Her A': SkyCoord.from_name("Hercules A"),
        #        'Cen A': SkyCoord.from_name("Centaurus A"),
        #        '?': SkyCoord.from_name("J101415.9+105106"),
        #        '3C295': SkyCoord.from_name("3C295"),
        #        'Moon': get_moon(obstime_astropy, location=station_earthlocation).transform_to(GCRS),
        'Sun': get_sun(obstime_astropy)
        #        '3C196': SkyCoord.from_name("3C196")
    }

    marked_bodies_lmn = {}
    for body_name, body_coord in marked_bodies.items():
        #print(body_name, body_coord.separation(zenith), body_coord.separation(zenith))
        if body_coord.transform_to(AltAz(location=station_earthlocation, obstime=obstime_astropy)).alt > 0:
            marked_bodies_lmn[body_name] = skycoord_to_lmn(marked_bodies[body_name], zenith)

    img = sky_imager(visibilities, baselines, freq, npix_l, npix_m)
    fig = make_sky_plot(img, marked_bodies_lmn, title=f"L2TS sky image for {station_name}",
                        subtitle=f"SB {subband:03d} ({freq / 1e6:.1f} MHz), {str(obstime)[:19]}");
    plt.savefig(f"{fname}.png", bbox_inches="tight")
