#!/usr/bin/env python3
import os
import re
import glob
import h5py

import numpy as np
import matplotlib.pyplot as plt

def read_lofar2_xst(fname):
    # Open file
    h5 = h5py.File(fname, "r")

    # Read keys
    keys = [key for key in h5.keys()]

    # Read visibilities
    l2_visibilities = np.array(h5[keys[0]]["values"])

    # Fill missing parts
    for i in range(12, 18):
        for j in range(0, 12):
            l2_visibilities[j, i] = l2_visibilities[i, j]

    # LOFAR 2.0 LBA_OUTER antenna selection (2022-11-17)
    ant_idx = np.array([72, 77, 83, 88, 90, 92, 93, 94, 95])

    # Offset
    ant_idx -= 48
    
    # Create LOFAR1 visibility cube
    cube = np.zeros((96, 96)).astype(np.complex128)
    for i in range(len(ant_idx)):
        for j in range(len(ant_idx)):
            cube[2 * ant_idx[i], 2 * ant_idx[j]] = l2_visibilities[2 * i, 2 * j]
            cube[2 * ant_idx[i] + 1, 2 * ant_idx[j] + 1] = l2_visibilities[2 * i + 1, 2 * j + 1]

    return cube
    
if __name__ == "__main__":
    fnames = sorted(glob.glob("../../data/XST_SB*.h5"))

    for xst_filename in fnames:
        cube = read_lofar2_xst(xst_filename)
        subband = int(re.search("SB([0-9]*)", xst_filename).groups(0)[0])
        obsdatestr = xst_filename.split("_")[-1].replace(".h5", "").replace("-", "")
        obsdatestr = f"{obsdatestr[:8]}_{obsdatestr[8:]}"
        outfname = f"test/{obsdatestr}_mode_1_xst_sb{subband:03d}.dat"
        cube.tofile(outfname)
        print(outfname)
