from os import listdir
from os.path import isfile,join
from datetime import datetime
import numpy as np
import h5py

def loadXST(fn0,sis,tstart,tend):
    D2=[]
    times=[]
    dateformat='%Y-%m-%dT%H:%M:%S'
#    XST_2022-10-28T16:16:06
    with h5py.File(fn0[:], 'r') as f:
      for s in f.keys():
        try:
         time=datetime.strptime(s[4:23],dateformat).timestamp()
         if time<tstart: continue
         if time>tend: continue
         D=np.array(f[s]['values'])
         D2.append(D[:,sis][sis])
         times.append(time)
        except:
            continue;
#    D2=np.array(D2)
    return D2,times
class XSTs():
    def __init__ (self,fpath):
        files=listdir(fpath)#[f for f in listdir(fpath)];
        files.sort()
        self.files = [f for f in files if isfile(join(fpath,f))]
        self.band=np.zeros([len(self.files)],dtype='int')
        self.times=[]
        dateformat='%Y-%m-%d-%H-%M-%S'
        for i,fn in enumerate(self.files):
            s=fn.split('_');
            self.band[i]=int(s[1][2:])
            ftime=s[2][:19]
            dtime=datetime.strptime(ftime, dateformat)
            self.times.append(dtime)
        #print(self.times)
        self.band_nums=np.unique(self.band)
        Nbands=len(self.band_nums)
        self.band_index=[np.where(self.band==b)[0] for b in self.band_nums]
        self.band_files=[[join(fpath,self.files[i]) for i in bi] for bi in self.band_index]
        self.band_times=[[self.times[i].timestamp() for i in bi] for bi in self.band_index]
        print("XST files=",len(self.files),", Bands=",self.band_nums)
    def getbandindex(self,band):
        b=np.where(self.band_nums==band)
        return None if len(b[0])<1 else b[0][0]
    def GetBand(self,band,t_start,t_end,sis):
        bandindex=self.getbandindex(band)
        if bandindex is None: return None
        t_start=t_start.timestamp()
        t_end=t_end.timestamp()
        #print(t_start,t_end)
        D=[]
        times=[]
        for i,tm in enumerate(self.band_times[bandindex]):
            if tm>t_end: continue
            if tm<t_start-60*60: continue
            fn=self.band_files[bandindex][i]
            print("Load XST",fn)
            D1,times1=loadXST(fn,sis,t_start,t_end)
            D+=D1
            times+=times1
        D=np.array(D)
        return D,times