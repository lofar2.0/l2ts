from datetime import datetime
import sys
import numpy as np
import h5py
from os import listdir
from os.path import isfile,join

def loadBST(fn0,tstart,tend):
    D2=[]
    dateformat='%Y-%m-%dT%H:%M:%S'
#    XST_2022-10-28T16:16:06
    with h5py.File(fn0[:], 'r') as f:
      for s in f.keys():
        try:
         time=datetime.strptime(s[4:23],dateformat).timestamp()
         if time<tstart: continue
         if time>tend: continue
         D=np.array(f[s]['values'])
         D2.append(D)
        except:
            continue;
#    D2=np.array(D2)
    return D2

def loadSST(fn0,tstart,tend,sis,bands):
    D2=[]
    dateformat='%Y-%m-%dT%H:%M:%S'
    with h5py.File(fn0[:], 'r') as f:
      for s in f.keys():
        try:
         time=datetime.strptime(s[4:23],dateformat).timestamp()
         if time<tstart: continue
         if time>tend: continue
         D=np.array(f[s]['values'])
         D2.append(D[sis][:,bands])
#         print(D2[-1].shape)
        except:
            continue;
#    D2=np.array(D2)
    return D2

def LoadL1sst(date,rcu,bands=range(512),path=''):
        fn=path+date+"_sst_rcu%03i.dat"%rcu
        print(fn)
        A=np.fromfile(fn,dtype='float')
        N=len(A)//512
        A=A.reshape([N,512])
        return A[:,bands]

class BSTs():
    def __init__ (self,fpath):
        files=listdir(fpath)#[f for f in listdir(fpath)];
        files.sort()
        self.files = [f for f in files if isfile(join(fpath,f))]
        self.times=[]
        dateformat='%Y-%m-%d-%H-%M-%S'
        for i,fn in enumerate(self.files):
            s=fn.split('_');
            ftime=s[1][:19]
            dtime=datetime.strptime(ftime, dateformat)
            self.times.append(dtime.timestamp())
        self.files=[join(fpath,f) for f in self.files]
    def getData(self,t_start,t_end):
        t_start=t_start.timestamp()
        t_end=t_end.timestamp()
        D=[]
        for i,tm in enumerate(self.times):
            if tm>t_end: continue
            if tm<t_start-60*60: continue
            fn=self.files[i]
            print("Load BST",fn)
            D+=loadBST(fn,t_start,t_end)
        D=np.array(D)
        return D
    def getData_avg(self,t_start,t_end,navg):
        D=self.getData(t_start,t_end)
        n1,n2,n3=D.shape
        na=n1//navg
        return np.mean(D[:na*navg].reshape([na,navg,n2,n3]),axis=1)
#    def getData_percentile(self,t_start,t_end,navg,percentile=50):
#        D=self.getData(t_start,t_end)
#        n1,n2,n3=D.shape
#        na=n1//navg
        return np.percentile(D[:na*navg].reshape([na,navg,n2,n3]),percentile,axis=1)
    def getData_percentile(self,t_start,t_end,navg,percentile=50):
        t_start=t_start.timestamp()
        t_end=t_end.timestamp()
        D=[]
        Davg=[]
        Nbands=488*4
        for i,tm in enumerate(self.times):
            if tm>t_end: continue
            if tm<t_start-60*60: continue
            fn=self.files[i]
            print("Load BST",fn)
            D+=loadBST(fn,t_start,t_end)
            n0=len(D)//navg
            if n0==0: continue
            Davg.append(np.percentile(np.array(D[:n0*navg]).reshape([n0,navg,Nbands]),percentile,axis=1))
            D=D[n0*navg:]
        Davg=np.concatenate(Davg,axis=0)
        n1,n2=Davg.shape
        return Davg.reshape([n1,2,n2//2])

class SSTs():
    def __init__ (self,fpath):
        files=listdir(fpath)#[f for f in listdir(fpath)];
        files.sort()
#        self.files = [f for f in files if isfile(join(fpath,f))]
        self.files = [f for f in files if isfile(join(fpath,f)) and (f[-3:]=='.h5')]
        self.times=[]
        dateformat='%Y-%m-%d-%H-%M-%S'
        for i,fn in enumerate(self.files):
            s=fn.split('_');
            ftime=s[1][:19]
            dtime=datetime.strptime(ftime, dateformat)
            #print(fn,dtime)
            self.times.append(dtime.timestamp())
        self.files=[join(fpath,f) for f in self.files]
    def getData(self,t_start,t_end,sis,bands=range(512)):
        t_start=t_start.timestamp()
        t_end=t_end.timestamp()
        D=[]
        for i,tm in enumerate(self.times):
            if tm>t_end: continue
            if tm<t_start-60*60: continue
            fn=self.files[i]
            print("Load SST",fn)
            try:
              D+=loadSST(fn,t_start,t_end,sis,bands)
            except:
              continue;
        D=np.array(D)
        return D        
#    def getData_avg(self,t_start,t_end,sis,navg):
#        D=self.getData(t_start,t_end,sis)
#        print(D.shape)
#        n1,n2,n3=D.shape
#        na=n1//navg
#        return np.mean(D[:na*navg].reshape([na,navg,n2,n3]),axis=1)

    def getData_avg(self,t_start,t_end,sis,navg):
        t_start=t_start.timestamp()
        t_end=t_end.timestamp()
        D=[]
        Davg=[]
        Nbands=512
        Nsi=len(sis)
        for i,tm in enumerate(self.times):
            if tm>t_end: continue
            if tm<t_start-60*60: continue
            fn=self.files[i]
            print("Load SST",fn)
            D+=loadSST(fn,t_start,t_end,sis,bands=range(512))
            n0=len(D)//navg
            if n0==0: continue
            Davg.append(np.mean(np.array(D[:n0*navg]).reshape([n0,navg,Nsi,Nbands]),axis=1))
            D=D[n0*navg:]
        Davg=np.concatenate(Davg,axis=0)
        return Davg
    def getData_percentile(self,t_start,t_end,sis,navg,percentile=50):
        t_start=t_start.timestamp()
        t_end=t_end.timestamp()
        D=[]
        Davg=[]
        Nbands=512
        Nsi=len(sis)
        for i,tm in enumerate(self.times):
            if tm>t_end: continue
            if tm<t_start-60*60: continue
            fn=self.files[i]
            print("Load SST",fn)
            D+=loadSST(fn,t_start,t_end,sis,bands=range(512))
            n0=len(D)//navg
            if n0==0: continue
            Davg.append(np.percentile(np.array(D[:n0*navg]).reshape([n0,navg,Nsi,Nbands]),percentile,axis=1))
            D=D[n0*navg:]
        Davg=np.concatenate(Davg,axis=0)
        return Davg

#    def getData_percentile(self,t_start,t_end,sis,navg,percentile=50):
#        D=self.getData(t_start,t_end,sis)
#        print(D.shape)
#        n1,n2,n3=D.shape
#        na=n1//navg
#        return np.percentile(D[:na*navg].reshape([na,navg,n2,n3]),percentile,axis=1)