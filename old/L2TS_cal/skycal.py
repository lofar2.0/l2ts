import numpy as np
if False:
    from pygdsm import GlobalSkyModel
    from pygdsm import GSMObserver
    NSIDE=512

if False:
    from pygdsm import GlobalSkyModel2016 as GlobalSkyModel
    from pygdsm import GSMObserver2016 as GSMObserver
    NSIDE=512

if True:
    from pygdsm import LowFrequencySkyModel as GlobalSkyModel
    from pygdsm import LFSMObserver as GSMObserver
    NSIDE=256


from functools import partial
import healpy as hp
import matplotlib.pyplot as plt
from astropy import units as u
from numpy.fft import fft,fftshift,ifft,ifftshift

from datetime import datetime,timedelta

from astropy.coordinates import SkyCoord, UnitSphericalRepresentation, EarthLocation, AltAz
from astropy.time import Time
from astropy import constants as const
from scipy import io

import h5py
from os import listdir
from os.path import isfile,join
from time import sleep
import csv

import numpy.ma as ma
from tqdm import tqdm  


from astropy.utils.iers import conf
conf.auto_max_age = None

def get_lm(nside=NSIDE,plot=False):
    npix=hp.nside2npix(nside)
    XYZ=np.array(hp.pix2vec(nside,range(npix)))#.T
#    mask = XYZ[0]<0
    l=XYZ[1]
    m=XYZ[2]
    if plot:
        hp.orthview(l,half_sky = True, title = 'l')
        hp.orthview(m,half_sky = True, title = 'm')
    return l,m 

def get_coord(nside=NSIDE,plot=False):
    npix=hp.nside2npix(nside)
    XYZ=np.array(hp.pix2vec(nside,range(npix)))#.T
    mask = XYZ[0]<0
    t=XYZ[0].copy();XYZ[0]=XYZ[2];XYZ[2]=t #swap X and Z
    XYZ=hp.vec2ang(XYZ.T)
    az_rad = np.ma.masked_array(XYZ[1], mask = mask )
    z_rad = np.ma.masked_array(XYZ[0], mask = mask )
    if plot:
        hp.orthview(az_rad,title='az')
        hp.orthview(z_rad,title='zenith')
    return az_rad,z_rad


class LBArad:
    def __init__ (self,path):
        self.path=path
#        self.alt_az_coords=alt_az_coords
    def filename(self,freq):
        return self.path+'lba_CS001_single_ended_pat_freq%imhz.mat'%freq
    def getbeam(self,freq,lbas):
        fn=self.filename(freq)
#        print("Loading",fn)
        file = io.loadmat(fn)
        frequency_beam = np.array(file["Freq"][0])
        phi_beam = np.array(file["Phi"][0])
        theta_beam = np.array(file["Theta"][0])
#        print(phi_beam,theta_beam)
        Vdiff_pol1_beam = np.array(file["Vdiff_pol1"][lbas])
        Vdiff_pol2_beam = np.array(file["Vdiff_pol2"][lbas])
        Nlba=len(lbas)
#        print("freq=",frequency_beam,"N phi=",len(phi_beam)," N theta",len(theta_beam),Vdiff_pol1_beam.shape)
        beam_pattern = np.ones((Nlba,2,91,73))
        beam_pattern[:,0] = (Vdiff_pol1_beam[:,0]*np.conjugate(Vdiff_pol1_beam[:,0])+Vdiff_pol1_beam[:,1]*np.conjugate(Vdiff_pol1_beam[:,1])).real
        beam_pattern[:,1] = (Vdiff_pol2_beam[:,0]*np.conjugate(Vdiff_pol2_beam[:,0])+Vdiff_pol2_beam[:,1]*np.conjugate(Vdiff_pol2_beam[:,1])).real
        return beam_pattern,theta_beam,phi_beam
    
def interpolate(patt,z1,az1,tz1,ta1):
    nz=len(z1)
    na=len(az1)
#    print(patt.shape,nz,na,tz1.shape,ta1.shape)
    patt=patt.reshape([nz*na])
    iz1=np.array(tz1,dtype='int')
    ia1=np.array(ta1/5,dtype='int')
    iz1-=iz1*(iz1<0)
    ia1-=ia1*(ia1<0)
    iz1-=iz1*(iz1>=nz-1)
    ia1-=ia1*(ia1>=na-1)
    wz=tz1-iz1
    wa=ta1/5-ia1
    it=iz1*na+ia1
    z3=(1-wz)*( (1-wa)*patt[it]+(wa)*patt[it+1] ) + (wz)*( (1-wa)*patt[it+na]+(wa)*patt[it+1+na] )
    return z3
def interpolateN(patt,z1,az1,tz1,ta1):
    nz=len(z1)
    na=len(az1)
#    print(patt.shape,nz,na,tz1.shape,ta1.shape)
    patt=patt.reshape([patt.shape[0],nz*na])
    iz1=np.array(tz1,dtype='int')
    ia1=np.array(ta1/5,dtype='int')
    iz1-=iz1*(iz1<0)
    ia1-=ia1*(ia1<0)
    iz1-=iz1*(iz1>=nz-1)
    ia1-=ia1*(ia1>=na-1)
    wz=tz1-iz1
    wa=ta1/5-ia1
    it=iz1*na+ia1
    z3=(1-wz)*( (1-wa)*patt[:,it]+(wa)*patt[:,it+1] ) + (wz)*( (1-wa)*patt[:,it+na]+(wa)*patt[:,it+1+na] )
    return z3

def CalcPower(freq,latlonel,dates,beamsky):
    (latitude, longitude, elevation) = latlonel
    Ntime=len(dates)
    pwr=np.zeros([2,Ntime])
    ov = GSMObserver()
    ov.lon = longitude
    ov.lat = latitude
    ov.elev = elevation
    for x,date0 in  tqdm(enumerate(dates)):
        ov.date = date0
        ov.generate(freq.value,obstime=Time(date0))    
        pwr[0,x]=np.sum( ov.observed_sky*beamsky[0])
        pwr[1,x]=np.sum( ov.observed_sky*beamsky[1])
    del ov;
    return pwr;

def CalcPowerN(freq,latlonel,dates,beamsky):
    Nlba=beam_sky[0].shape[0];
    print(Nlba)
    (latitude, longitude, elevation) = latlonel
    Ntime=len(dates)
    pwr=np.zeros([Nlba,2,Ntime])
    ov = GSMObserver()
    ov.lon = longitude
    ov.lat = latitude
    ov.elev = elevation
    for x,date0 in  tqdm(enumerate(dates)):
        ov.date = date0
        ov.generate(freq.value,obstime=Time(date0))    
        for i in range(Nlba):
            pwr[i,0,x]=np.sum( ov.observed_sky*beamsky[0][i])
            pwr[i,1,x]=np.sum( ov.observed_sky*beamsky[1][i])
    del ov;
    return pwr;

def compute_uv_coordinates(ant_pos, dtype=np.float32):
    r'''
    Return an array of all uv-coordinates. The array has a shape of
    (num_ant, num_ant, 2), and should be of type `dtype`, and unit 
    `astropy.units.m`
    '''
    num_ant = len(ant_pos)
    result = np.zeros((num_ant, num_ant, 2), dtype=dtype)*u.m
    for ant1 in range(num_ant):
        for ant2 in range(num_ant):
            result[ant1, ant2] = ant_pos[ant1] - ant_pos[ant2]
    return result

def calculate_visibility(dates,latlonel,freq,uv,beamsky):
    Nlba=uv.shape[0]
    (latitude, longitude, elevation) = latlonel
    Ntime=len(dates)
    pwr=np.zeros([Nlba,2,Ntime])
    ov = GSMObserver()
    ov.lon = longitude
    ov.lat = latitude
    ov.elev = elevation
    f = freq.to(u.Hz).value
    l,m=get_lm()
    visibilities=np.zeros([Ntime,Nlba,Nlba,2],dtype='complex')
    for i1 in (range(Nlba)):
      for i2 in range(Nlba):
        if i2>i1: continue;
#        print('Calculate',i1,i2)
        if i1==i2:
            exp_power=1
        else:
            uv_l = uv[i1,i2,:].to(u.m).value
            uv_l = uv_l*f/const.c.value
            exp_power = -2*np.pi*1j*(uv_l[0]*l+uv_l[1]*m)
            exp_power=np.exp(exp_power)
        for x,date0 in (enumerate(dates)):
            ov.date = date0
            ov.generate(freq.value,obstime=Time(date0))    
            visibilities[x,i1,i2,0] = np.sum(ov.observed_sky*beamsky[0]*exp_power)
            visibilities[x,i1,i2,1] = np.sum(ov.observed_sky*beamsky[1]*exp_power)
    del ov;    
    return visibilities