#!/usr/bin/env python3
import sys
import requests

import numpy as np
import matplotlib.pyplot as plt

from scipy import interpolate
from scipy import optimize

from sgp4.api import Satrec

import astropy.units as u
from astropy.io import ascii
from astropy.time import Time
from astropy.coordinates import SkyCoord, FK5, GCRS, EarthLocation, AltAz
from astropy.coordinates import CartesianRepresentation, CartesianDifferential, TEME

import lofarantpos
from lofarantpos.db import LofarAntennaDatabase

if __name__ == "__main__":
    # Input settings
    sourcefname = "sources.csv"
    tstart_isot = "2022-12-06T00:00:00"
    duration_s = 86400
    satno = 40069

    # Satellites
    # NOAA 15:    25338 APT at 137.620MHz
    # NOAA 18:    28654 APT at 137.912MHz
    # NOAA 19:    33591 APT at 137.100MHz
    # Meteor M-2: 40069 LRPT at 137.100MHz
    
    # Read sources
    d = ascii.read(sourcefname, format="csv")
    psrc = SkyCoord(ra=d["ra"], dec=d["dec"], unit=("hourangle", "deg"), frame="icrs")
    names = d["name"]
    
    # Compute satellite passes
    t = Time(tstart_isot, format="isot", scale="utc") + np.arange(duration_s) * u.s
    
    # Download TLE
    resp = requests.get(f"http://celestrak.org/NORAD/elements/gp.php?CATNR={satno:05d}")
    lines = resp.content.decode("utf-8").splitlines()

    # Satellite position and velocity
    sat = Satrec.twoline2rv(lines[1], lines[2])
    _, teme_p, teme_v = sat.sgp4_array(t.jd1, t.jd2)
    teme_p = CartesianRepresentation(teme_p.T*u.km)
    teme = TEME(teme_p, obstime=t)

    # Reference location
    db = LofarAntennaDatabase()
    loc = EarthLocation.from_geocentric(*(db.phase_centres["CS001HBA0"] * u.m))
    
    # To altaz
    gloc = loc.get_gcrs(obstime=t)
    g = teme.transform_to(GCRS(obstime=t, obsgeoloc=gloc.cartesian))
    a = teme.transform_to(AltAz(obstime=t, location=loc))

    # Find maximum pass
    idx = np.argmax(a.alt.degree)
    tmax = t[idx]
    amax = a[idx].alt.degree
    dt = 600 * u.s
    if amax < 60:
        print("No passes above 60 degrees found")
        sys.exit()
    
    # Select points above altitude limit
    c = (a.alt.degree > 30) & (np.abs((t - tmax).to(u.s)) < dt)

    # Loop over sources
    for p, name in zip(psrc, names):
        # Setup interpolating functions
        fdec = interpolate.interp1d(t[c].mjd, g[c].dec.degree - p.dec.degree)
        faz = interpolate.interp1d(t[c].mjd, a[c].az.degree)
        falt = interpolate.interp1d(t[c].mjd, a[c].alt.degree)

        # Time extrema
        tmin, tmax = np.min(t[c]), np.max(t[c])

        # Find pass
        mjdpass = optimize.bisect(fdec, tmin.mjd, tmax.mjd)
        tpass = Time(mjdpass, format="mjd")
        apass = AltAz(az=faz(mjdpass) * u.deg, alt=falt(mjdpass) * u.deg, obstime=tpass, location=loc)
        gpass = apass.transform_to(GCRS(obstime=tpass))

        sra = gpass.ra.to_string(unit="hourangle", sep=":", precision=2, pad=True)
        sdec = gpass.dec.to_string(unit="deg", sep=":", precision=1, alwayssign=True, pad=True)
        
        print(f"{name} {tpass.isot} {apass.az.degree:7.3f} {apass.alt.degree:6.3f} {sra} {sdec}")
