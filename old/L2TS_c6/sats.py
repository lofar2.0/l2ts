#!pip install satellitetle
from satellite_tle import fetch_tle_from_celestrak,fetch_latest_tles
from sgp4.api import Satrec
from sgp4.api import SGP4_ERRORS
from astropy.time import Time
from astropy.coordinates import TEME, CartesianDifferential, CartesianRepresentation
from astropy import units as u
from astropy.coordinates import EarthLocation, AltAz
from astropy.coordinates import ITRS
import numpy as np
from datetime import datetime

def getSatFile(sat_name,t_start,t_end):
#    print(t_start)
    datestr=t_start.strftime('%Y-%m-%d')
#    D=np.load('CS1HBA0_'+datestr+'.npz')
    D=np.load('C'+datestr+'.npz')
    if not(sat_name) in D['sat_names']:
        print("Sat names=",D['sat_names'])
        return None;
    satindex=np.where(D['sat_names']==sat_name)[0][0]
    alts=D['alts'][satindex]
    azs=D['azs'][satindex]
    t0=datetime.strptime(datestr, '%Y-%m-%d').timestamp()
    t_start=t_start.timestamp()-t0
    t_end=t_end.timestamp()-t0
    tstep=24*60*60/len(alts)
    i0=int(t_start/tstep)
    i1=int(t_end/tstep)

    return alts[i0:i1],azs[i0:i1]
def calcSat(norad_id,LOFARloc,t_start,t_end,t_step):
    name = fetch_latest_tles([norad_id])
    name,s,t = name[norad_id][1]
    satellite = Satrec.twoline2rv(s, t)
    Ntime=int((t_end.timestamp()-t_start.timestamp())/t_step)
    print("Sat name=",name,t_start,t_end,Ntime)
    delta_time = np.arange(Ntime)*t_step*u.second #15*24*2
    times=Time(t_start)+delta_time
    alt=[]
    az=[]
    for t in times:
      error_code, teme_p, teme_v = satellite.sgp4(t.jd1, t.jd2)  # in km and km/s
      teme_p = CartesianRepresentation(teme_p*u.km)
      teme_v = CartesianDifferential(teme_v*u.km/u.s)
      teme = TEME(teme_p.with_differentials(teme_v), obstime=t)
      itrs = teme.transform_to(ITRS(obstime=t))  
      location2 = itrs.earth_location
      aa = teme.transform_to(AltAz(obstime=t, location=LOFARloc))  
      alt.append(aa.alt.value)
      az.append(aa.az.value)
    return(alt,az)