from datetime import datetime
import sys
import numpy as np
import h5py
from os import listdir
from os.path import isfile,join
import matplotlib.pyplot as plt

def loadBST(fn0,tstart,tend):
    D2=[]
    dateformat='%Y-%m-%dT%H:%M:%S'
#    XST_2022-10-28T16:16:06
    with h5py.File(fn0[:], 'r') as f:
      for s in f.keys():
        try:
         time=datetime.strptime(s[4:23],dateformat).timestamp()
         if time<tstart: continue
         if time>tend: continue
         D=np.array(f[s]['values'])
         D2.append(D)
        except:
            continue;
#    D2=np.array(D2)
    return D2


def LoadL1sst(date,rcu,bands=range(512),path=''):
        fn=path+date+"_sst_rcu%03i.dat"%rcu
        print(fn)
        A=np.fromfile(fn,dtype='float')
        N=len(A)//512
        A=A.reshape([N,512])
        return A[:,bands]

class BSTs():
    def __init__ (self,fpath):
        files=listdir(fpath)#[f for f in listdir(fpath)];
        files.sort()
        self.files = [f for f in files if isfile(join(fpath,f))]
        self.times=[]
        dateformat='%Y-%m-%d-%H-%M-%S'
        for i,fn in enumerate(self.files):
            s=fn.split('_');
            ftime=s[1][:19]
            dtime=datetime.strptime(ftime, dateformat)
            self.times.append(dtime.timestamp())
        self.files=[join(fpath,f) for f in self.files]
    def getData(self,t_start,t_end):
        t_start=t_start.timestamp()
        t_end=t_end.timestamp()
        D=[]
        for i,tm in enumerate(self.times):
            if tm>t_end: continue
            if tm<t_start-60*60: continue
            fn=self.files[i]
            print("Load BST",fn)
            D+=loadBST(fn,t_start,t_end)
        D=np.array(D)
        return D
    def getData_avg(self,t_start,t_end,navg):
        D=self.getData(t_start,t_end)
        n1,n2,n3=D.shape
        na=n1//navg
        return np.mean(D[:na*navg].reshape([na,navg,n2,n3]),axis=1)
    def getData_percentile(self,t_start,t_end,navg,percentile=50):
        D=self.getData(t_start,t_end)
        n1,n2,n3=D.shape
        na=n1//navg
        return np.percentile(D[:na*navg].reshape([na,navg,n2,n3]),percentile,axis=1)
        
class SSTs():
    def __init__ (self,fpath, start_si=0, stop_si=19, band = 'LBA'):
        files=listdir(fpath)#[f for f in listdir(fpath)];
        files.sort()
#        self.files = [f for f in files if isfile(join(fpath,f))]
        self.files = [f for f in files if isfile(join(fpath,f)) and (f[-3:]=='.h5')]
        self.times=[]
        dateformat='%Y-%m-%d-%H-%M-%S'
        for i,fn in enumerate(self.files):
            s=fn.split('_');
            ftime=s[1][start_si:stop_si]
            dtime=datetime.strptime(ftime, dateformat)
            #print(fn,dtime)
            self.times.append(dtime.timestamp())
        self.files=[join(fpath,f) for f in self.files]
        self.D=[]
        self.GainADC=(10*np.log10(100e6)+20*np.log10(2**12)) #XST to dBFS
        self.hour_of_day=[]
        self.band = band
        if self.band == 'HBA':
            self.freqs = 200-(np.arange(512)/512*100)
        else:
            self.freqs = np.arange(512)/512*100
        self.input_label=[]
        self.START_TIME=""


        
    def loadSST(self, fn0,tstart,tend,sis,bands):
        D2=[]
        dateformat='%Y-%m-%dT%H:%M:%S'
        with h5py.File(fn0[:], 'r') as f:
          for s in f.keys():
            try:
             time=datetime.strptime(s[4:23],dateformat).timestamp()
             if time<tstart: continue
             if time>tend: continue
             D=np.array(f[s]['values'])
             D2.append(D[sis][:,bands])
    #         print(D2[-1].shape)
            except:
                continue;
    #    D2=np.array(D2)
        return D2

    def getData(self,t_start,t_end,sis,bands=range(512)):
        t_start=t_start.timestamp()
        t_end=t_end.timestamp()
        for i,tm in enumerate(self.times):
            if tm>t_end: continue
            if tm<t_start-60*60: continue
            fn=self.files[i]
            print("Load SST",fn)
            self.D+=self.loadSST(fn,t_start,t_end,sis,bands)
        self.D=np.array(self.D)
#        return D        
    
    def getData_avg(self,t_start,t_end,sis,navg):
        self.getData(t_start,t_end,sis)
        print(self.D.shape)
        n1,n2,n3=self.D.shape
        na=n1//navg
        self.D = np.mean(self.D[:na*navg].reshape([na,navg,n2,n3]),axis=1)
        
        
    def getData_percentile(self,t_start,t_end,sis,navg,percentile=50):
        t_start=t_start.timestamp()
        t_end=t_end.timestamp()
        D=[]
        Davg=[]
        Nbands=512
        Nsi=len(sis)
        for i,tm in enumerate(self.times):
            if tm>t_end: continue
            if tm<t_start-60*60: continue
            fn=self.files[i]
            print("Load SST",fn)
            D+=self.loadSST(fn,t_start,t_end,sis,bands=range(512))
            n0=len(D)//navg
            if n0==0: continue
            Davg.append(np.percentile(np.array(D[:n0*navg]).reshape([n0,navg,Nsi,Nbands]),percentile,axis=1))
            D=D[n0*navg:]
        self.D=np.concatenate(Davg,axis=0)
#        return Davg
        

#    def getData_percentile(self,t_start,t_end,sis,navg,percentile=50):
#        self.getData(t_start,t_end,sis)
##        print(D.shape)
#        n1,n2,n3=self.D.shape
#        na=n1//navg
#        self.D = np.percentile(self.D[:na*navg].reshape([na,navg,n2,n3]),percentile,axis=1)
    
    def plot_sst_time(self, f_measurement = 69.9):
        tstep_min=10
        self.hour_of_day=np.arange(self.D.shape[0])/60*tstep_min
        fbin_loc = np.argmin(np.abs(self.freqs-f_measurement))

        plt.figure(dpi=300)
        for i in [fbin_loc]:
            Pwr=(10*np.log10(self.D[:,::2,i]))-self.GainADC
            Pwr-=np.mean(Pwr,axis=0)[np.newaxis,:]
            for l in range(len(self.input_label)//2):
                plt.plot(self.hour_of_day,Pwr[:,l],label=self.input_label[l*2]);
        plt.gca().set_prop_cycle(None)
        for i in [fbin_loc]:
            Pwr=(10*np.log10(self.D[:,1::2,i]))-self.GainADC
            Pwr-=np.mean(Pwr,axis=0)[np.newaxis,:]
            for l in range(len(self.input_label)//2):
                plt.plot(self.hour_of_day,Pwr[:,l],':',label=self.input_label[1+(l*2)]);
        plt.ylabel('Normalised Power (dB)')
        plt.legend(fontsize='xx-small',ncol=6)
        plt.xlabel('Hours since ' + self.START_TIME)
        plt.title("L2TS SST power (%.1f MHz)"%self.freqs[fbin_loc])
        plt.grid()

    def plot_sst_frequency(self, time_slot=1):
        plt.figure(dpi=300)
        FS=200
        FFT_POINTS=512
        STATION_NAME = 'CS001c'
        if self.band == 'HBA':
            f_as = [FS-((FS*cnt)/(2*FFT_POINTS)) for cnt in range(FFT_POINTS)]
        else:
            f_as = [(FS*cnt)/(2*FFT_POINTS) for cnt in range(FFT_POINTS)]
        for cnt, f_si in enumerate(self.D[time_slot,:,:]):
            plot_data = 10 * np.log10(f_si + 1) - 128 - 6 * 4
            plt_label = self.input_label[cnt]
            plt.plot(f_as, plot_data, label = plt_label)
        plt.grid()
        if self.band == 'HBA':
            plt.xlim([100,200])
        else:
            plt.xlim([0,100])
        plt.legend(fontsize='xx-small',ncol=5)
        plt.xlabel("Frequency (MHz)")
        plt.ylabel("Power (dB full scale)")
        if self.band == 'HBA':
            plt.title(f"Subband Statistics LBA {STATION_NAME}; {self.START_TIME}")
        else:
            plt.title(f"Subband Statistics HBA {STATION_NAME}; {self.START_TIME}")
        plt.show() 

    def plot_sst_waterfall(self, clim = None, si_nr=1):
        plt.figure(dpi=300)
        #plt.imshow(10*np.log10(D[:,1,:])-GainADC,vmin=-80,vmax=-60)
        yas = self.hour_of_day #range(len(D[:,1,1]))
        plt.pcolor(self.freqs, yas, 10*np.log10(self.D[:,(si_nr),:])-self.GainADC) #,vmin=-80,vmax=-60)
        plt.colorbar(label = 'Power dB')
        plt.xlabel('Frequency MHz')
        plt.ylabel('Hours since ' + self.files[0][4:-4])
        plt.title(f" Spectrogam of HBA input nr {self.input_label[si_nr]}")
        print(clim)
        if clim != None:
            plt.clim(clim)
        plt.show()
        #plt.savefig("./results/sst_lba_waterval.png")